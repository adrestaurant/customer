package com.frf.frfcustomer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;
import com.frf.frfcustomer.ExpandableAdapters.CuisineExpandableListAdapter;
import com.frf.frfcustomer.ExpandableAdapters.DietPrefsExpandableListAdapter;
import com.frf.frfcustomer.ExpandableAdapters.FoodCategoryExpandableListAdapter;
import com.frf.frfcustomer.ExpandableAdapters.MealExpandableListAdapter;
import com.frf.frfcustomer.ExpandableModels.DataItem;
import com.frf.frfcustomer.ExpandableModels.SubCategoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FilterMenuActivity extends AppCompatActivity implements IGPSActivity {

    private ExpandableListView dietTypeList, cuisineList, foodCategoryList, mealList;
    private EditText min_price, max_price;
    private TimePicker start_time, end_time;
    private Button applyButton;

    private ArrayList<DataItem> cuisineArCategory,dietPrefsArCategory, foodCategoryArCategory, mealArCategory;
    private ArrayList<SubCategoryItem>cuisineArSubCategory,dietPrefsArSubCategory,foodCategoryArSubCategory,mealArSubCategory;

    private ArrayList<HashMap<String, String>> cuisineParentItems, dietPrefsParentItems,foodCategoryParentItems, mealParentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> cuisineChildItems, dietPrefsChildItems, foodCategoryChildItems, mealChildItems;
    private DietPrefsExpandableListAdapter dietPrefsExpandableListAdapter;
    private FoodCategoryExpandableListAdapter foodCategoryExpandableListAdapter;
    private MealExpandableListAdapter mealExpandableListAdapter;
    private CuisineExpandableListAdapter cuisineExpandableListAdapter;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private String rid;
    private HashMap<String, String> map;

    private JSONArray dietPrefs, userDietPrefs, cuisines, foodCategories, meals;
    private JSONObject filters;
    private String selectedFoodCategories, selectedMeals, selectedDietPrefs, selectedCuisines, selectedStartTime, selectedEndTime,selectedMinPrice,selectedMaxPrice;
    private int childHeight;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_menus);

        gps = new GPS(this);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        min_price = findViewById(R.id.min_price);
        min_price.setText(preferenceHelper.getMinPrice());

        max_price = findViewById(R.id.max_price);
        max_price.setText(preferenceHelper.getMaxPrice());

        start_time = findViewById(R.id.start_time);
        start_time.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            start_time.setHour(0);
            start_time.setMinute(0);
        }
        else{
            start_time.setCurrentHour(0);
            start_time.setCurrentMinute(0);
        }

        end_time = findViewById(R.id.end_time);
        end_time.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            end_time.setHour(0);
            end_time.setMinute(0);
        }
        else{
            end_time.setCurrentHour(0);
            end_time.setCurrentMinute(0);
        }

        map = new HashMap<>();

        //add the filter params to the hashmap that stores the request data
        Intent intent = getIntent();
        rid = intent.getStringExtra(AndyConstants.Params.RID);
        map.put(AndyConstants.Params.RID, rid);//get the rid

        applyButton = findViewById(R.id.applyButton);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //get selected/entered values
                    getAppliedFilters();
                    //make new intent to show results
                    Intent intent = new Intent(FilterMenuActivity.this, ListingMenuActivity.class);

                    intent.putExtra(AndyConstants.Params.FOOD_CATEGORIES, selectedFoodCategories);
                    intent.putExtra(AndyConstants.Params.MEAL, selectedMeals);
                    intent.putExtra(AndyConstants.Params.RID, rid);
                    intent.putExtra(AndyConstants.Params.DIET_PREFERENCES, selectedDietPrefs);
                    intent.putExtra(AndyConstants.Params.CUISINE, selectedCuisines);
                    intent.putExtra(AndyConstants.Params.MIN_PRICE, selectedMinPrice);
                    intent.putExtra(AndyConstants.Params.MAX_PRICE, selectedMaxPrice);
                    intent.putExtra(AndyConstants.Params.START_TIME, selectedStartTime);
                    intent.putExtra(AndyConstants.Params.END_TIME, selectedEndTime);

                    preferenceHelper.putActivityChanged(true);

                    startActivity(intent);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        new FetchFiltersAsyncTask().execute();

    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }





    /**
     * Fetches the lists for the expandables from the server
     */
    private class FetchFiltersAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(FilterMenuActivity.this)) {
                Toast.makeText(FilterMenuActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(FilterMenuActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.MENUSINDEX);

                HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.UID, preferenceHelper.getUID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("filtersjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }


    private void setupReferences(String result) {

        Log.d("received refs", result);
        if (parseContent.isSuccess(result)) {
            filters = parseContent.getDataObj(result);
        }else {//if something goes wrong with the retrieval, put them back at the login screen with an error toast
            Toast.makeText(FilterMenuActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(FilterMenuActivity.this, ListingMenuActivity.class);
            intent.putExtra(AndyConstants.Params.RID, rid);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }
        Log.d("filters", String.valueOf(filters));



//        FOODCATEGORIES
        foodCategoryList = findViewById(R.id.foodCategoryList);
        Log.d("foodCategoryList", String.valueOf(foodCategoryList));

        DataItem foodCategoryItem = new DataItem();
        foodCategoryItem.setCategoryId("foodCategory");
        foodCategoryItem.setCategoryName("Food Category");

        foodCategoryArCategory = new ArrayList<>();
        foodCategoryArSubCategory = new ArrayList<>();
        foodCategoryParentItems = new ArrayList<>();
        foodCategoryChildItems = new ArrayList<>();

        try {
            foodCategories = filters.getJSONArray(AndyConstants.Params.FOOD_CATEGORIES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < foodCategories.length(); i++) {
            JSONObject foodCategory = null;
            try {
                foodCategory = foodCategories.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("foodCategory");
                subCategoryItem.setSubId(String.valueOf(foodCategory.getInt(AndyConstants.Params.FC_ID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(foodCategory.getString(AndyConstants.Params.DB_FC_NAME));
                foodCategoryArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        foodCategoryItem.setSubCategory(foodCategoryArSubCategory);
        foodCategoryArCategory.add(foodCategoryItem);

        for(DataItem data : foodCategoryArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            foodCategoryChildItems.add(childArrayList);
            foodCategoryParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = foodCategoryParentItems;
        ExpandableConstantManager.childItems = foodCategoryChildItems;

        Log.d("TAG", "foodCategory parent: "+foodCategoryParentItems);
        Log.d("TAG", "foodCategory children: "+foodCategoryChildItems);


        foodCategoryExpandableListAdapter = new FoodCategoryExpandableListAdapter(this,foodCategoryParentItems,foodCategoryChildItems,false);

        foodCategoryList.setAdapter(foodCategoryExpandableListAdapter);

        Log.d("TAG", "foodCategory parent: "+foodCategoryParentItems);
        Log.d("TAG", "foodCategory children: "+foodCategoryChildItems);


        foodCategoryList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("foodCategory children","count= "+foodCategoryList.getChildCount());
                for (int i = 0; i < foodCategoryList.getChildCount(); i++) {
                    childHeight = foodCategoryList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + foodCategoryList.getDividerHeight();
                }

                Log.d("count", String.valueOf(foodCategoryExpandableListAdapter.getChildrenCount(groupPosition)));
                foodCategoryList.getLayoutParams().height = (height) * (foodCategoryExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        foodCategoryList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                foodCategoryList.getLayoutParams().height = childHeight;
            }
        });


        //        MEALS
        mealList = findViewById(R.id.mealList);
        Log.d("mealList", String.valueOf(mealList));

        DataItem mealItem = new DataItem();
        mealItem.setCategoryId("meal");
        mealItem.setCategoryName("Meal");

        mealArCategory = new ArrayList<>();
        mealArSubCategory = new ArrayList<>();
        mealParentItems = new ArrayList<>();
        mealChildItems = new ArrayList<>();

        try {
            meals = filters.getJSONArray(AndyConstants.Params.MEALS);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < meals.length(); i++) {
            JSONObject meal = null;
            try {
                meal = meals.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("meal");
                subCategoryItem.setSubId(String.valueOf(meal.getInt(AndyConstants.Params.MEAL_ID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(meal.getString(AndyConstants.Params.DB_MEAL_NAME));
                mealArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        mealItem.setSubCategory(mealArSubCategory);
        mealArCategory.add(mealItem);

        for(DataItem data : mealArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            mealChildItems.add(childArrayList);
            mealParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = mealParentItems;
        ExpandableConstantManager.childItems = mealChildItems;

        Log.d("TAG", "meal parent: "+mealParentItems);
        Log.d("TAG", "meal children: "+mealChildItems);


        mealExpandableListAdapter = new MealExpandableListAdapter(this,mealParentItems,mealChildItems,false);

        mealList.setAdapter(mealExpandableListAdapter);

        Log.d("TAG", "meal parent: "+mealParentItems);
        Log.d("TAG", "meal children: "+mealChildItems);


        mealList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("meal children","count= "+mealList.getChildCount());
                for (int i = 0; i < mealList.getChildCount(); i++) {
                    childHeight = mealList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + mealList.getDividerHeight();
                }

                Log.d("count", String.valueOf(mealExpandableListAdapter.getChildrenCount(groupPosition)));
                mealList.getLayoutParams().height = (height) * (mealExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        mealList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                mealList.getLayoutParams().height = childHeight;
            }
        });


//        CUISINES
        cuisineList = findViewById(R.id.cuisineList);
        Log.d("cuisineList", String.valueOf(cuisineList));

        DataItem cuisineItem = new DataItem();
        cuisineItem.setCategoryId("cuisine");
        cuisineItem.setCategoryName("Cuisine");

        cuisineArCategory = new ArrayList<>();
        cuisineArSubCategory = new ArrayList<>();
        cuisineParentItems = new ArrayList<>();
        cuisineChildItems = new ArrayList<>();

        try {
            cuisines = filters.getJSONArray(AndyConstants.Params.CUISINE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < cuisines.length(); i++) {
            JSONObject cuisine = null;
            try {
                cuisine = cuisines.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("cuisine");
                subCategoryItem.setSubId(String.valueOf(cuisine.getInt(AndyConstants.Params.CU_ID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(cuisine.getString(AndyConstants.Params.CUISINE_NAME));
                cuisineArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        cuisineItem.setSubCategory(cuisineArSubCategory);
        cuisineArCategory.add(cuisineItem);

        for(DataItem data : cuisineArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            cuisineChildItems.add(childArrayList);
            cuisineParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = cuisineParentItems;
        ExpandableConstantManager.childItems = cuisineChildItems;

        Log.d("TAG", "cuisine parent: "+cuisineParentItems);
        Log.d("TAG", "cuisine children: "+cuisineChildItems);


        cuisineExpandableListAdapter = new CuisineExpandableListAdapter(this,cuisineParentItems,cuisineChildItems,false);

        cuisineList.setAdapter(cuisineExpandableListAdapter);

        Log.d("TAG", "cuisine parent: "+cuisineParentItems);
        Log.d("TAG", "cuisine children: "+cuisineChildItems);


        cuisineList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("cuisine children","count= "+cuisineList.getChildCount());
                for (int i = 0; i < cuisineList.getChildCount(); i++) {
                    childHeight = cuisineList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + cuisineList.getDividerHeight();
                }

                Log.d("count", String.valueOf(cuisineExpandableListAdapter.getChildrenCount(groupPosition)));
                cuisineList.getLayoutParams().height = (height) * (cuisineExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        cuisineList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                cuisineList.getLayoutParams().height = childHeight;
            }
        });


//        //DIET PREFERENCES

        dietTypeList = findViewById(R.id.dietTypeList);

        DataItem dietPrefsItem = new DataItem();
        dietPrefsItem.setCategoryId("diet_preferences");
        dietPrefsItem.setCategoryName("Diet Preferences");

        dietPrefsArCategory = new ArrayList<>();
        dietPrefsArSubCategory = new ArrayList<>();
        dietPrefsParentItems = new ArrayList<>();
        dietPrefsChildItems = new ArrayList<>();

        try {
            dietPrefs = filters.getJSONArray(AndyConstants.Params.DIET_PREFERENCES);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            userDietPrefs = filters.getJSONArray(AndyConstants.Params.USER_DIET_PREFERENCES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < dietPrefs.length(); i++) {
            JSONObject dietPref = null;
            try {
                dietPref = dietPrefs.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("diet_preferences");
                subCategoryItem.setSubId(String.valueOf(dietPref.getInt(AndyConstants.Params.DT_ID)));

                for(int a=0; a < userDietPrefs.length(); a++){
                    JSONObject userDietPref = userDietPrefs.getJSONObject(a);
                    if(userDietPref.getInt(AndyConstants.Params.DT_ID) == dietPref.getInt(AndyConstants.Params.DT_ID)){//if the diet pref is one of the users preferred diets (from their profile)
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                        break;
                    }
                    else{
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }
                }

                subCategoryItem.setSubCategoryName(dietPref.getString(AndyConstants.Params.DT_NAME));
                dietPrefsArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        dietPrefsItem.setSubCategory(dietPrefsArSubCategory);
        dietPrefsArCategory.add(dietPrefsItem);

        for(DataItem data : dietPrefsArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            dietPrefsChildItems.add(childArrayList);
            dietPrefsParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = dietPrefsParentItems;
        ExpandableConstantManager.childItems = dietPrefsChildItems;

        Log.d("TAG", "dietPrefs parent: "+dietPrefsParentItems);
        Log.d("TAG", "dietPrefs children: "+dietPrefsChildItems);

        dietPrefsExpandableListAdapter = new DietPrefsExpandableListAdapter(this,dietPrefsParentItems,dietPrefsChildItems,false);

        dietTypeList.setAdapter(dietPrefsExpandableListAdapter);

        Log.d("TAG", "dietPrefs parent: "+dietPrefsParentItems);
        Log.d("TAG", "dietPrefs children: "+dietPrefsChildItems);

        dietTypeList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("dietType children","count= "+dietTypeList.getChildCount());
                for (int i = 0; i < dietTypeList.getChildCount(); i++) {
                    childHeight = dietTypeList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + dietTypeList.getDividerHeight();
                }

                Log.d("count", String.valueOf(dietPrefsExpandableListAdapter.getChildrenCount(groupPosition)));
                dietTypeList.getLayoutParams().height = (height) * (dietPrefsExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        dietTypeList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                dietTypeList.getLayoutParams().height = childHeight;
            }
        });

    }

    public void getAppliedFilters()throws IOException, JSONException {

        //have to determine what times were chosen and which checkboxes checked

        //selectedFoodCategories
        for (int i = 0; i < FoodCategoryExpandableListAdapter.parentItems.size(); i++ ) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < FoodCategoryExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = FoodCategoryExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(FoodCategoryExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }

            }
            selectedFoodCategories = String.valueOf(children);
            Log.d("selectedFoodCategories",selectedFoodCategories);
        }

        //selectedMeals
        for (int i = 0; i < MealExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < MealExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = MealExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(MealExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }
            }
            selectedMeals = String.valueOf(children);
            Log.d("selectedMeals",selectedMeals);
        }


        //selectedCuisines
        for (int i = 0; i < CuisineExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < CuisineExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = CuisineExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(CuisineExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }
            }
            selectedCuisines = String.valueOf(children);
            Log.d("selectedCuisines",selectedCuisines);
        }

        //selectedDietPrefs
        for (int i = 0; i < DietPrefsExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < DietPrefsExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }
            }
            selectedDietPrefs = String.valueOf(children);
            Log.d("selectedDietPrefs",selectedDietPrefs);
        }

        //selectedStartTime
        int start_hour, start_minute;

        if (Build.VERSION.SDK_INT >= 23 ){
            start_hour = start_time.getHour();
            start_minute = start_time.getMinute();
        }
        else{
            start_hour = start_time.getCurrentHour();
            start_minute = start_time.getCurrentMinute();
        }

        String desiredStartTime = start_hour +":"+ start_minute;
        if(desiredStartTime.equals("00:00") || desiredStartTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
            selectedStartTime = null;
        }
        else{
            selectedStartTime = desiredStartTime;
        }



        //selectedCloseTime
        int end_hour, end_minute;

        if (Build.VERSION.SDK_INT >= 23 ){
            end_hour = end_time.getHour();
            end_minute = end_time.getMinute();
        }
        else{
            end_hour = end_time.getCurrentHour();
            end_minute = end_time.getCurrentMinute();
        }

        String desiredEndTime = end_hour +":"+ end_minute;
        if(desiredEndTime.equals("00:00") || desiredEndTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
            selectedEndTime = null;
        }
        else{
            selectedEndTime = desiredEndTime;
        }

        //selectedMinPrice
        selectedMinPrice = min_price.getText().toString();
        preferenceHelper.putMinPrice(selectedMinPrice);

        //selectedMaxPrice
        selectedMaxPrice = max_price.getText().toString();
        preferenceHelper.putMaxPrice(selectedMaxPrice);


    }
}
