package com.frf.frfcustomer;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfcustomer.CardAdapters.DishCardAdapter;
import com.frf.frfcustomer.CardModels.DishModel;
import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailsMenuActivity extends AppCompatActivity implements IGPSActivity {

    private TextView restaurant_name, meal_name, start_time, end_time;
    private ImageView menuImage;
    private Button filterButton;
    private RecyclerView recyclerView;
    private ArrayList<DishModel> dishModelArrayList;
    private DishCardAdapter dishCardAdapter;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private String menu_id;

    private HashMap<String, String> map;
    private JSONObject menu_info;
    private JSONArray dishes;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_menu);

        Intent intent = getIntent();
        menu_id =  intent.getStringExtra(AndyConstants.Params.MENU_ID);
        Log.d("det men menu_id",menu_id);

        gps = new GPS(this);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        restaurant_name = findViewById(R.id.restaurant_name);
        meal_name = findViewById(R.id.meal_name);
        menuImage = findViewById(R.id.menuImage);
        start_time = findViewById(R.id.start_time);
        end_time = findViewById(R.id.end_time);

        filterButton = findViewById(R.id.filterButton);

        recyclerView = findViewById(R.id.recyclerView);

        map = new HashMap<>();

        //need to get filter values from the intent and map them for the request
        map.put(AndyConstants.Params.CUISINE, intent.getStringExtra(AndyConstants.Params.CUISINE));
        map.put(AndyConstants.Params.DIET_PREFERENCES, intent.getStringExtra(AndyConstants.Params.DIET_PREFERENCES));
        if(map.get(AndyConstants.Params.DIET_PREFERENCES) == null){
            map.put(AndyConstants.Params.UID, preferenceHelper.getUID());
        }
        map.put(AndyConstants.Params.MIN_PRICE, intent.getStringExtra(AndyConstants.Params.MIN_PRICE));
        map.put(AndyConstants.Params.MAX_PRICE, intent.getStringExtra(AndyConstants.Params.MAX_PRICE));
        map.put(AndyConstants.Params.FOOD_CATEGORIES, intent.getStringExtra(AndyConstants.Params.FOOD_CATEGORIES));


        new FetchDetailsAsyncTask().execute();

        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Toast.makeText(DetailsMenuActivity.this, dishModelArrayList.get(position).getDishName(), Toast.LENGTH_SHORT).show();
                String d_id = dishModelArrayList.get(position).getDID();
                Intent intent = new Intent(DetailsMenuActivity.this, DetailsDishActivity.class);
                intent.putExtra(AndyConstants.Params.D_ID,d_id);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsMenuActivity.this, FilterDishActivity.class);
                intent.putExtra(AndyConstants.Params.MENU_ID, menu_id);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });
    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }






    /**
     * Fetches the details (based on menu_id)
     */
    private class FetchDetailsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(DetailsMenuActivity.this)) {
                Toast.makeText(DetailsMenuActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(DetailsMenuActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.MENUSDETAILS);

                map.put(AndyConstants.Params.MENU_ID, menu_id);

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("result", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            try {
                menu_info = parseContent.getDataObj(result).getJSONObject("menu_info");//get the menu info from the results
                restaurant_name.setText(Html.fromHtml(menu_info.getString(AndyConstants.Params.RESTAURANT_NAME)));//set the restaurant name
                meal_name.setText(Html.fromHtml(menu_info.getString("meal_name"))+" Menu");//set the meal name
                //get the image
                Picasso.get().load(Uri.parse(AndyConstants.ServiceType.IMAGES + menu_info.getString("menu_image")))
                        .resize(600, 300)
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder_error)
                        .into(menuImage);

                start_time.setText(menu_info.getString("start_time"));//set the start time
                end_time.setText(menu_info.getString("end_time"));//set the end time

                if(parseContent.getDataObj(result).getJSONArray("dishes").length() == 0){//if there are no active dishes show the text view
                    showNoDishes();
                }
                else {//if there are results, show them
                    populateDishList(result);
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

    private void showNoDishes(){
        TextView noDishesTV = findViewById(R.id.noDishesTV);
        noDishesTV.setVisibility(View.VISIBLE);
    }


    private ArrayList<DishModel> populateDishList(String result){

        dishModelArrayList = new ArrayList<DishModel>();

        Log.d("received rest info", result);
        if (parseContent.isSuccess(result)) {
            try {
                dishes = parseContent.getDataObj(result).getJSONArray("dishes");
            } catch (JSONException e) {
                e.printStackTrace();
            }

            for(int i = 0; i < dishes.length(); i++) {
                JSONObject dish = null;
                try {
                    dish = dishes.getJSONObject(i);
                    DishModel dishModel = new DishModel();

                    dishModel.setDishName(dish.getString(AndyConstants.Params.DISH_NAME));
                    dishModel.setDietType(dish.getString(AndyConstants.Params.DIET_TYPE));
                    dishModel.setCuisine(dish.getString(AndyConstants.Params.CUISINE));
                    dishModel.setFoodCategory(dish.getString(AndyConstants.Params.FOOD_CATEGORY));
                    dishModel.setPrice(dish.getString(AndyConstants.Params.PRICE));
                    dishModel.setDID(dish.getString(AndyConstants.Params.D_ID));
                    dishModel.setImageUrl(dish.getString(AndyConstants.Params.DISH_IMAGE));

                    dishModelArrayList.add(dishModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.d("number of dishes", dishModelArrayList.size() + "");

            dishCardAdapter = new DishCardAdapter(this,dishModelArrayList);
            recyclerView.setAdapter(dishCardAdapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        }else {//if something goes wrong with the retrieval, put them back at the menu listing screen with an error toast
            Toast.makeText(DetailsMenuActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(DetailsMenuActivity.this, ListingMenuActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            intent.putExtra(AndyConstants.Params.MENU_ID,menu_id);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }


        return dishModelArrayList;
    }


    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }
}
