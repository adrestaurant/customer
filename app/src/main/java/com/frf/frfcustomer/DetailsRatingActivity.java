package com.frf.frfcustomer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.RatingBar;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfcustomer.CardModels.RatingsModel;
import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;

import org.json.JSONException;

import java.util.ArrayList;
import java.util.HashMap;

public class DetailsRatingActivity extends AppCompatActivity implements IGPSActivity {

    private RatingBar rating;
    private EditText comment;
    private Button submitButton;

    String rtng, cmnt;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private HashMap<String, String> map;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_rating);

        gps = new GPS(this);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        rating = findViewById(R.id.rating);
        comment = findViewById(R.id.comment);
        submitButton = findViewById(R.id.submitButton);

        map = new HashMap<>();

        //add the filter params to the hashmap that stores the request data
        Intent intent = getIntent();

        map.put(AndyConstants.Params.RID, intent.getStringExtra(AndyConstants.Params.RID));//get the rid


        new FetchRatingAsyncTask().execute();

        rating.setOnRatingBarChangeListener(new RatingBar.OnRatingBarChangeListener(){
            @Override
            public void onRatingChanged(RatingBar ratingBar, float v, boolean b) {
                Toast.makeText(DetailsRatingActivity.this, v +"/5", Toast.LENGTH_SHORT).show();
            }
        });

        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                submitRating();
            }
        });
    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }





    /**
     * Fetches the rating
     */
    private class FetchRatingAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(DetailsRatingActivity.this)) {
                Toast.makeText(DetailsRatingActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(DetailsRatingActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.RATINGSGETONE);

                map.put(AndyConstants.Params.UID, preferenceHelper.getUID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            afterRetrieve(response);
            return response;
        }

        protected void afterRetrieve(String result) {
            Log.d("ratingsjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            Log.d("data", String.valueOf(parseContent.getData(result)));
            Log.d("data null?", String.valueOf(parseContent.getData(result).isNull(0)));


            if(!parseContent.getData(result).isNull(0)){//if there is a previous  review
                populateRating(result);
            }
        }

    }

    private void populateRating(String result) {
        if (parseContent.isSuccess(result)) {
            try {
                rtng = parseContent.getData(result).getJSONObject(0).getString(AndyConstants.Params.RATING);
                cmnt = String.valueOf(Html.fromHtml(parseContent.getData(result).getJSONObject(0).getString(AndyConstants.Params.COMMENT)));
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
        rating.setRating(Float.parseFloat(rtng));
        comment.setText(cmnt);
    }

    public void submitRating(){
        if (!AndyUtils.isNetworkAvailable(DetailsRatingActivity.this)) {
            Toast.makeText(DetailsRatingActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(DetailsRatingActivity.this);

        rtng = String.valueOf(rating.getRating());
        map.put(AndyConstants.Params.RATING, rtng);//get the rating
        map.put(AndyConstants.Params.COMMENT, String.valueOf(comment.getText()));

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.RATINGSINSERT);

                    map.put(AndyConstants.Params.UID, preferenceHelper.getUID());


                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                if(parseContent.isSuccess(result)) {
                    onTaskCompleted(result);
                }
                else{
                    AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

                    Toast.makeText(DetailsRatingActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();//get the error message

                    rating.setRating(Float.parseFloat(rtng));
                }
            }
        }.execute();
    }
    private void onTaskCompleted(String response) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

        if (parseContent.isSuccess(response)) {
            Toast.makeText(DetailsRatingActivity.this, "Feedback received!", Toast.LENGTH_SHORT).show();//get the message (not an error)
        }
    }


}
