package com.frf.frfcustomer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TimePicker;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;
import com.frf.frfcustomer.ExpandableAdapters.CuisineExpandableListAdapter;
import com.frf.frfcustomer.ExpandableAdapters.DietPrefsExpandableListAdapter;
import com.frf.frfcustomer.ExpandableAdapters.ParishExpandableListAdapter;
import com.frf.frfcustomer.ExpandableAdapters.VillageExpandableListAdapter;
import com.frf.frfcustomer.ExpandableModels.DataItem;
import com.frf.frfcustomer.ExpandableModels.SubCategoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

public class FilterRestaurantActivity extends AppCompatActivity implements IGPSActivity {

    private ExpandableListView parishList, villageList, dietTypeList, cuisineList;
    private EditText nearme, min_price, max_price;
    private TimePicker open_time, close_time;
    private Button applyButton;

    private ArrayList<DataItem> parishArCategory,villageArCategory,cuisineArCategory,dietPrefsArCategory;
    private ArrayList<SubCategoryItem> parishArSubCategory,villageArSubCategory,cuisineArSubCategory,dietPrefsArSubCategory;

    private ArrayList<HashMap<String, String>> parishParentItems,villageParentItems, cuisineParentItems, dietPrefsParentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> parishChildItems,villageChildItems, cuisineChildItems, dietPrefsChildItems;
    private DietPrefsExpandableListAdapter dietPrefsExpandableListAdapter;
    private ParishExpandableListAdapter parishExpandableListAdapter;
    private VillageExpandableListAdapter villageExpandableListAdapter;
    private CuisineExpandableListAdapter cuisineExpandableListAdapter;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private JSONArray parishes, villages, dietPrefs, userDietPrefs, cuisines;
    private JSONObject filters;
    private String selectedParishes, selectedVillages, selectedDietPrefs, selectedCuisines, selectedOpenTime, selectedCloseTime,selectedMinPrice,selectedMaxPrice;
    private int childHeight;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_filter_restaurants);

        gps = new GPS(this);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        nearme = findViewById((R.id.nearme));

        min_price = findViewById(R.id.min_price);
        min_price.setText(preferenceHelper.getMinPrice());

        max_price = findViewById(R.id.max_price);
        max_price.setText(preferenceHelper.getMaxPrice());

        open_time = findViewById(R.id.open_time);
        open_time.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            open_time.setHour(0);
            open_time.setMinute(0);
        }
        else{
            open_time.setCurrentHour(0);
            open_time.setCurrentMinute(0);
        }

        close_time = findViewById(R.id.close_time);
        close_time.setIs24HourView(true);
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            close_time.setHour(0);
            close_time.setMinute(0);
        }
        else{
            close_time.setCurrentHour(0);
            close_time.setCurrentMinute(0);
        }

        applyButton = findViewById(R.id.applyButton);
        applyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    //get selected/entered values
                    getAppliedFilters();
                    //make new intent to show results
                    Intent intent = new Intent(FilterRestaurantActivity.this, ListingRestaurantActivity.class);

                    intent.putExtra(AndyConstants.Params.PARISHES, selectedParishes);
                    intent.putExtra(AndyConstants.Params.VILLAGES, selectedVillages);
                    intent.putExtra(AndyConstants.Params.NEARME, nearme.getText().toString());
                    intent.putExtra(AndyConstants.Params.DIET_PREFERENCES, selectedDietPrefs);
                    intent.putExtra(AndyConstants.Params.CUISINE, selectedCuisines);
                    intent.putExtra(AndyConstants.Params.MIN_PRICE, selectedMinPrice);
                    intent.putExtra(AndyConstants.Params.MAX_PRICE, selectedMaxPrice);
                    intent.putExtra(AndyConstants.Params.OPEN_TIME, selectedOpenTime);
                    intent.putExtra(AndyConstants.Params.CLOSE_TIME, selectedCloseTime);

                    preferenceHelper.putActivityChanged(true);

                    startActivity(intent);
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        new FetchFiltersAsyncTask().execute();

    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }



    /**
     * Fetches the lists for the expandables from the server
     */
    private class FetchFiltersAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(FilterRestaurantActivity.this)) {
                Toast.makeText(FilterRestaurantActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(FilterRestaurantActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.RESTAURANTINDEX);

                HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.UID, preferenceHelper.getUID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("filtersjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }


    private void setupReferences(String result) {

        Log.d("received refs", result);
        if (parseContent.isSuccess(result)) {
            filters = parseContent.getDataObj(result);
        }else {//if something goes wrong with the retrieval, put them back at the listing screen with an error toast
            Toast.makeText(FilterRestaurantActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(FilterRestaurantActivity.this, ListingRestaurantActivity.class);
            intent.putExtra(AndyConstants.Params.UID,preferenceHelper.getUID());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }
        Log.d("filters", String.valueOf(filters));



//        arSubCategory = new ArrayList<>();
//

//PARISHES
        parishList = findViewById(R.id.parishList);
        Log.d("parishList", String.valueOf(parishList));

        DataItem parishItem = new DataItem();
        parishItem.setCategoryId("parishes");
        parishItem.setCategoryName("Parishes");

        parishArCategory = new ArrayList<>();
        parishArSubCategory = new ArrayList<>();
        parishParentItems = new ArrayList<>();
        parishChildItems = new ArrayList<>();

        try {
            parishes = filters.getJSONArray(AndyConstants.Params.PARISHES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < parishes.length(); i++) {
            JSONObject parish = null;
            try {
                parish = parishes.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("parishes");
                subCategoryItem.setSubId(String.valueOf(parish.getInt(AndyConstants.Params.PID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(parish.getString(AndyConstants.Params.PARISH_NAME));
                parishArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        parishItem.setSubCategory(parishArSubCategory);
        parishArCategory.add(parishItem);

        for(DataItem data : parishArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            parishChildItems.add(childArrayList);
            parishParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = parishParentItems;
        ExpandableConstantManager.childItems = parishChildItems;

        Log.d("TAG", "parish parent: "+parishParentItems);
        Log.d("TAG", "parish children: "+parishChildItems);

        parishExpandableListAdapter = new ParishExpandableListAdapter(this,parishParentItems,parishChildItems,false);

        parishList.setAdapter(parishExpandableListAdapter);

        Log.d("TAG", "parish parent: "+parishParentItems);
        Log.d("TAG", "parish children: "+parishChildItems);

        parishList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("parish children","count= "+parishList.getChildCount());
                for (int i = 0; i < parishList.getChildCount(); i++) {
                    childHeight = parishList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + parishList.getDividerHeight();
                }

                Log.d("count", String.valueOf(parishExpandableListAdapter.getChildrenCount(groupPosition)));
                parishList.getLayoutParams().height = (height) * (parishExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        parishList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                parishList.getLayoutParams().height = childHeight;
            }
        });

        //VILLAGES
        villageList = findViewById(R.id.villageList);


        DataItem villageItem = new DataItem();
        villageItem.setCategoryId("villages");
        villageItem.setCategoryName("Villages");

        villageArCategory = new ArrayList<>();
        villageArSubCategory = new ArrayList<>();
        villageParentItems = new ArrayList<>();
        villageChildItems = new ArrayList<>();

        try {
            villages = filters.getJSONArray(AndyConstants.Params.VILLAGES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < villages.length(); i++) {
            JSONObject village = null;
            try {
                village = villages.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("villages");
                subCategoryItem.setSubId(String.valueOf(village.getInt(AndyConstants.Params.VID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(village.getString(AndyConstants.Params.VILLAGE_NAME));
                villageArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        villageItem.setSubCategory(villageArSubCategory);
        villageArCategory.add(villageItem);

        for(DataItem data : villageArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            villageChildItems.add(childArrayList);
            villageParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = villageParentItems;
        ExpandableConstantManager.childItems = villageChildItems;

        Log.d("TAG", "village parent: "+villageParentItems);
        Log.d("TAG", "village children: "+villageChildItems);

        villageExpandableListAdapter = new VillageExpandableListAdapter(this,villageParentItems,villageChildItems,false);

        villageList.setAdapter(villageExpandableListAdapter);

        Log.d("TAG", "village parent: "+villageParentItems);
        Log.d("TAG", "village children: "+villageChildItems);

        villageList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("village children","count= "+villageList.getChildCount());
                for (int i = 0; i < villageList.getChildCount(); i++) {
                    childHeight = villageList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + villageList.getDividerHeight();
                }

                Log.d("count", String.valueOf(villageExpandableListAdapter.getChildrenCount(groupPosition)));
                villageList.getLayoutParams().height = (height) * (villageExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        villageList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                villageList.getLayoutParams().height = childHeight;
            }
        });

//        CUISINES
       cuisineList = findViewById(R.id.cuisineList);
        Log.d("cuisineList", String.valueOf(cuisineList));

        DataItem cuisineItem = new DataItem();
        cuisineItem.setCategoryId("cuisine");
        cuisineItem.setCategoryName("Cuisine");

        cuisineArCategory = new ArrayList<>();
        cuisineArSubCategory = new ArrayList<>();
        cuisineParentItems = new ArrayList<>();
        cuisineChildItems = new ArrayList<>();

        try {
            cuisines = filters.getJSONArray(AndyConstants.Params.CUISINE);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < cuisines.length(); i++) {
            JSONObject cuisine = null;
            try {
                cuisine = cuisines.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("cuisine");
                subCategoryItem.setSubId(String.valueOf(cuisine.getInt(AndyConstants.Params.CU_ID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(cuisine.getString(AndyConstants.Params.CUISINE_NAME));
                cuisineArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        cuisineItem.setSubCategory(cuisineArSubCategory);
        cuisineArCategory.add(cuisineItem);

        for(DataItem data : cuisineArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            cuisineChildItems.add(childArrayList);
            cuisineParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = cuisineParentItems;
        ExpandableConstantManager.childItems = cuisineChildItems;

        Log.d("TAG", "cuisine parent: "+cuisineParentItems);
        Log.d("TAG", "cuisine children: "+cuisineChildItems);


        cuisineExpandableListAdapter = new CuisineExpandableListAdapter(this,cuisineParentItems,cuisineChildItems,false);

        cuisineList.setAdapter(cuisineExpandableListAdapter);

        Log.d("TAG", "cuisine parent: "+cuisineParentItems);
        Log.d("TAG", "cuisine children: "+cuisineChildItems);


        cuisineList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("cuisine children","count= "+cuisineList.getChildCount());
                for (int i = 0; i < cuisineList.getChildCount(); i++) {
                    childHeight = cuisineList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + cuisineList.getDividerHeight();
                }

                Log.d("count", String.valueOf(cuisineExpandableListAdapter.getChildrenCount(groupPosition)));
                cuisineList.getLayoutParams().height = (height) * (cuisineExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        cuisineList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                cuisineList.getLayoutParams().height = childHeight;
            }
        });


//        //DIET PREFERENCES

        dietTypeList = findViewById(R.id.dietTypeList);

        DataItem dietPrefsItem = new DataItem();
        dietPrefsItem.setCategoryId("diet_preferences");
        dietPrefsItem.setCategoryName("Diet Preferences");

        dietPrefsArCategory = new ArrayList<>();
        dietPrefsArSubCategory = new ArrayList<>();
        dietPrefsParentItems = new ArrayList<>();
        dietPrefsChildItems = new ArrayList<>();

        try {
            dietPrefs = filters.getJSONArray(AndyConstants.Params.DIET_PREFERENCES);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            userDietPrefs = filters.getJSONArray(AndyConstants.Params.USER_DIET_PREFERENCES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < dietPrefs.length(); i++) {
            JSONObject dietPref = null;
            try {
                dietPref = dietPrefs.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("diet_preferences");
                subCategoryItem.setSubId(String.valueOf(dietPref.getInt(AndyConstants.Params.DT_ID)));

                for(int a=0; a < userDietPrefs.length(); a++){
                    JSONObject userDietPref = userDietPrefs.getJSONObject(a);
                    if(userDietPref.getInt(AndyConstants.Params.DT_ID) == dietPref.getInt(AndyConstants.Params.DT_ID)){//if the diet pref is one of the users preferred diets (from their profile)
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                        break;
                    }
                    else{
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }
                }

                subCategoryItem.setSubCategoryName(dietPref.getString(AndyConstants.Params.DT_NAME));
                dietPrefsArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        dietPrefsItem.setSubCategory(dietPrefsArSubCategory);
        dietPrefsArCategory.add(dietPrefsItem);

        for(DataItem data : dietPrefsArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            dietPrefsChildItems.add(childArrayList);
            dietPrefsParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = dietPrefsParentItems;
        ExpandableConstantManager.childItems = dietPrefsChildItems;

        Log.d("TAG", "dietPrefs parent: "+dietPrefsParentItems);
        Log.d("TAG", "dietPrefs children: "+dietPrefsChildItems);

        dietPrefsExpandableListAdapter = new DietPrefsExpandableListAdapter(this,dietPrefsParentItems,dietPrefsChildItems,false);

        dietTypeList.setAdapter(dietPrefsExpandableListAdapter);

        Log.d("TAG", "dietPrefs parent: "+dietPrefsParentItems);
        Log.d("TAG", "dietPrefs children: "+dietPrefsChildItems);

        dietTypeList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("dietType children","count= "+dietTypeList.getChildCount());
                for (int i = 0; i < dietTypeList.getChildCount(); i++) {
                    childHeight = dietTypeList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + dietTypeList.getDividerHeight();
                }

                Log.d("count", String.valueOf(dietPrefsExpandableListAdapter.getChildrenCount(groupPosition)));
                dietTypeList.getLayoutParams().height = (height) * (dietPrefsExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        dietTypeList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                dietTypeList.getLayoutParams().height = childHeight;
            }
        });

    }


    public void getAppliedFilters()throws IOException, JSONException {

        //have to determine what times were chosen and which checkboxes checked
        // selectedParishes, selectedVillages, selecteddietPrefs, selectedCuisines, selectedOpenTime, selectedCloseTime

        //selectedParishes
        for (int i = 0; i < ParishExpandableListAdapter.parentItems.size(); i++ ) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < ParishExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = ParishExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(ParishExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }

            }
            selectedParishes = String.valueOf(children);
            Log.d("selectedParishes",selectedParishes);
        }

        //selectedVillages
        for (int i = 0; i < VillageExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < VillageExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = VillageExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(VillageExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }
            }
            selectedVillages = String.valueOf(children);
            Log.d("selectedVillages",selectedVillages);
        }


        //selectedCuisines
        for (int i = 0; i < CuisineExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < CuisineExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = CuisineExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(CuisineExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }
            }
            selectedCuisines = String.valueOf(children);
            Log.d("selectedCuisines",selectedCuisines);
        }

        //selectedDietPrefs
        for (int i = 0; i < DietPrefsExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < DietPrefsExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }
            }
            selectedDietPrefs = String.valueOf(children);
            Log.d("selectedDietPrefs",selectedDietPrefs);
        }

        //selectedOpenTime
        int open_hour, open_minute;

        if (Build.VERSION.SDK_INT >= 23 ){
            open_hour = open_time.getHour();
            open_minute = open_time.getMinute();
        }
        else{
            open_hour = open_time.getCurrentHour();
            open_minute = open_time.getCurrentMinute();
        }

        String desiredOpenTime = open_hour +":"+ open_minute;
        if(desiredOpenTime.equals("00:00") || desiredOpenTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
            selectedOpenTime = null;
        }
        else{
            selectedOpenTime = desiredOpenTime;
        }



        //selectedCloseTime
        int close_hour, close_minute;

        if (Build.VERSION.SDK_INT >= 23 ){
            close_hour = close_time.getHour();
            close_minute = close_time.getMinute();
        }
        else{
            close_hour = close_time.getCurrentHour();
            close_minute = close_time.getCurrentMinute();
        }

        String desiredCloseTime = close_hour +":"+ close_minute;
        if(desiredCloseTime.equals("00:00") || desiredCloseTime.equals("0:0")){//in case they didn't choose a time and the value is still the default value
            selectedCloseTime = null;
        }
        else{
            selectedCloseTime = desiredCloseTime;
        }

        //selectedMinPrice
        selectedMinPrice = min_price.getText().toString();
        preferenceHelper.putMinPrice(selectedMinPrice);

        //selectedMaxPrice
        selectedMaxPrice = max_price.getText().toString();
        preferenceHelper.putMaxPrice(selectedMaxPrice);


    }

}
