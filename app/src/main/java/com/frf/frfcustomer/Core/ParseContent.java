package com.frf.frfcustomer.Core;

import android.app.Activity;
import android.util.Base64;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class ParseContent {

    private final String KEY_SUCCESS = "success";
    private final String KEY_MSG = "message";
    private final String KEY_DATA = "data";
    private Activity activity;
    PreferenceHelper preferenceHelper;

    public ParseContent(Activity activity) {
        this.activity = activity;
        preferenceHelper = new PreferenceHelper(activity);

    }

    public boolean isSuccess(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.optString(KEY_SUCCESS).equals("true")) {
                return true;
            } else {

                return false;
            }

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return false;
    }

    public String getErrorMessage(String response) {
        Log.d("message resp", "mr:"+response);
        try {
            JSONObject jsonObject = new JSONObject(response);
            return jsonObject.getString(KEY_MSG);

        } catch (JSONException e) {
            e.printStackTrace();
        }
        return "No data";
    }

    public JSONArray getData(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_SUCCESS).equals("true")) {
                JSONArray dataArray = jsonObject.getJSONArray(KEY_DATA);
                return dataArray;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public JSONObject getDataObj(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_SUCCESS).equals("true")) {
                JSONObject dataObject = jsonObject.getJSONObject(KEY_DATA);
                return dataObject;
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return null;
    }

    public void saveInfo(String response) {
        try {
            JSONObject jsonObject = new JSONObject(response);
            if (jsonObject.getString(KEY_SUCCESS).equals("true")) {
                JSONArray dataArray = jsonObject.getJSONArray(KEY_DATA);
                for (int i = 0; i < dataArray.length(); i++) {
                    JSONObject dataobj = dataArray.getJSONObject(i);
                    preferenceHelper.putUID(dataobj.getString(AndyConstants.Params.UID));
                    preferenceHelper.putUsername(dataobj.getString(AndyConstants.Params.USERNAME));
                    preferenceHelper.putPassword(dataobj.getString(AndyConstants.Params.PASSWORD));

                    //making the userpass param for the header "username:password"
                    String userpass = dataobj.getString(AndyConstants.Params.USERNAME) + ":" + dataobj.getString(AndyConstants.Params.PASSWORD);
                    byte[] data = userpass.getBytes();
                    String base64UserPass = Base64.encodeToString(data, Base64.DEFAULT);
                    preferenceHelper.putUserPass(base64UserPass);
                }
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }

    }

}
