package com.frf.frfcustomer.Core;

import android.Manifest;
import android.app.Activity;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.util.Log;

import androidx.core.app.ActivityCompat;

public class GPS {

    private IGPSActivity main;

    // Helper for GPS-Position
    private LocationListener mlocListener;
    private LocationManager mlocManager;

    private boolean isRunning;

    // Used in checking for runtime permissions.
    private static final int REQUEST_PERMISSIONS_REQUEST_CODE = 34;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_INTERVAL_IN_MILLISECONDS = 10000;

    /**
     * The desired interval for location updates. Inexact. Updates may be more or less frequent.
     */
    private static final long UPDATE_DISTANCE_IN_METERS = 15;

    private PreferenceHelper preferenceHelper;

    public GPS(IGPSActivity main) {
        this.main = main;

        preferenceHelper = new PreferenceHelper((Context) this.main);

        // GPS Position
        mlocManager = (LocationManager) ((Activity) this.main).getSystemService(Context.LOCATION_SERVICE);
        mlocListener = new MyLocationListener(main);


        //check for permissions
        if (ActivityCompat.checkSelfPermission((Context) this.main, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) this.main,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
        }
        if (ActivityCompat.checkSelfPermission((Context) this.main, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) this.main,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
        }


        if(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) == null) {
            Log.d("location provider:","NETWORK");
            mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, UPDATE_INTERVAL_IN_MILLISECONDS, UPDATE_DISTANCE_IN_METERS, mlocListener);
            if(preferenceHelper.getLongitude().equals("") || preferenceHelper.getLatitude().equals("")){
                Log.d("NETWORK","last location: "+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)));
                Log.d("NETWORK", "last long"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude()));
                Log.d("NETWORK", "last lat"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude()));

                preferenceHelper.putLongitude(String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude()));
                preferenceHelper.putLatitude(String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude()));
            }
        }
        else{
            Log.d("location provider:","GPS");
            mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL_IN_MILLISECONDS, UPDATE_DISTANCE_IN_METERS, mlocListener);

            if(preferenceHelper.getLongitude().equals("") || preferenceHelper.getLatitude().equals("")){
                Log.d("GPS", "last location"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)));
                Log.d("GPS", "last long"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude()));
                Log.d("GPS", "last lat"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude()));

                preferenceHelper.putLongitude(String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude()));
                preferenceHelper.putLatitude(String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude()));
            }
        }

        // GPS Position END
        this.isRunning = true;


    }


    public void stopGPS() {
        if(isRunning) {
            mlocManager.removeUpdates(mlocListener);
            this.isRunning = false;
        }
    }

    public void resumeGPS() {

        //check for permissions
        if (ActivityCompat.checkSelfPermission((Context) this.main, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) this.main,new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
        }

        if (ActivityCompat.checkSelfPermission((Context) this.main, Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions((Activity) this.main,new String[]{Manifest.permission.ACCESS_COARSE_LOCATION}, REQUEST_PERMISSIONS_REQUEST_CODE);
        }

        if(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER) == null) {
            Log.d("location provider:","NETWORK");
            mlocManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, UPDATE_INTERVAL_IN_MILLISECONDS, UPDATE_DISTANCE_IN_METERS, mlocListener);
            if(preferenceHelper.getLongitude().equals("") || preferenceHelper.getLatitude().equals("")){
                Log.d("NETWORK","last location: "+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER)));
                Log.d("NETWORK", "last long"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude()));
                Log.d("NETWORK", "last lat"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude()));

                preferenceHelper.putLongitude(String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLongitude()));
                preferenceHelper.putLatitude(String.valueOf(mlocManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER).getLatitude()));
            }
        }
        else{
            Log.d("location provider:","GPS");
            mlocManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, UPDATE_INTERVAL_IN_MILLISECONDS, UPDATE_DISTANCE_IN_METERS, mlocListener);

            if(preferenceHelper.getLongitude().equals("") || preferenceHelper.getLatitude().equals("")){
                Log.d("GPS", "last location"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER)));
                Log.d("GPS", "last long"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude()));
                Log.d("GPS", "last lat"+ String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude()));

                preferenceHelper.putLongitude(String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLongitude()));
                preferenceHelper.putLatitude(String.valueOf(mlocManager.getLastKnownLocation(LocationManager.GPS_PROVIDER).getLatitude()));
            }
        }
    }

    public boolean isRunning() {
        return this.isRunning;
    }

    public class MyLocationListener implements LocationListener {

        private final String TAG = MyLocationListener.class.getSimpleName();

        private Context context;
        private Activity activity;


        private PreferenceHelper preferenceHelper;

        public MyLocationListener(IGPSActivity main) {
            this.context = (Context) main;
            this.activity = (Activity) main;
            preferenceHelper = new PreferenceHelper(this.context);
        }

        @Override
        public void onLocationChanged(Location loc) {

            if(preferenceHelper.getActivityChanged().equals(true)){//if the change was triggered by a change of activity
                Log.d(TAG,"location change triggered by activity change");
                preferenceHelper.putActivityChanged(false);
            }
            else if(preferenceHelper.getFromLogin().equals(true)){
                Log.d(TAG,"location change triggered going to rest list from login");
                preferenceHelper.putFromLogin(false);
                new NotificationHelper(context, activity);
            }
            else if(preferenceHelper.getActivityChanged().equals(false)) {
                Log.d(TAG,"location actually changed");

                Log.d(TAG, "activity= " + activity);
                Log.d(TAG, "activitybool = " + (activity instanceof com.frf.frfcustomer.LoginActivity));

                Log.d(TAG, "sharedLong: " + preferenceHelper.getLongitude());
                Log.d(TAG, "Long: " + String.valueOf(loc.getLongitude()));
                Log.d(TAG, "Long bool: " + String.valueOf(Double.valueOf(preferenceHelper.getLongitude()).equals(loc.getLongitude())));
                Log.d(TAG, "sharedLat: " + preferenceHelper.getLatitude());
                Log.d(TAG, "Lat: " + String.valueOf(loc.getLatitude()));
                Log.d(TAG, "Lat bool: " + String.valueOf(Double.valueOf(preferenceHelper.getLatitude()).equals(loc.getLatitude())));

                if (!Double.valueOf(preferenceHelper.getLongitude()).equals(loc.getLongitude()) || !Double.valueOf(preferenceHelper.getLatitude()).equals(loc.getLatitude())) {//if the location actually changed
                    Log.d(TAG, "location changed inside onlocationchanged");
                    Log.d(TAG, "GPS-Longitude: " + loc.getLongitude());
                    Log.d(TAG, "GPS-Latitude: " + loc.getLatitude());

                    preferenceHelper.putLongitude(String.valueOf(loc.getLongitude()));
                    preferenceHelper.putLatitude(String.valueOf(loc.getLatitude()));

                    if (!(activity instanceof com.frf.frfcustomer.SignupActivity) &&
                            !(activity instanceof com.frf.frfcustomer.LoginActivity) &&
                            !(activity instanceof com.frf.frfcustomer.SplashActivity)) {//once it's not the sign up, log in or splash activities
                        new NotificationHelper(context, activity);
                    }

                } else {
                    Log.d(TAG, "location unchanged inside onlocationchanged");
                }
            }
        }

        @Override
        public void onProviderDisabled(String provider) {
            GPS.this.main.displayGPSSettingsDialog();
        }

        @Override
        public void onProviderEnabled(String provider) {

        }

        @Override
        public void onStatusChanged(String provider, int status, Bundle extras) {

        }

    }

}
