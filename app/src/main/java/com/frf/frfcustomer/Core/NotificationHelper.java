package com.frf.frfcustomer.Core;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.media.MediaPlayer;
import android.os.AsyncTask;
import android.os.Bundle;
import android.text.Html;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AlertDialog;

import com.frf.frfcustomer.DetailsMenuActivity;
import com.frf.frfcustomer.DetailsRestaurantActivity;
import com.frf.frfcustomer.DirectionsActivity;
import com.frf.frfcustomer.R;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.HashMap;

import static androidx.core.content.ContextCompat.startActivity;

public class NotificationHelper {


    private AlertDialog notificationDialogBuilder;
    private Context context;
    private Activity activity;
    private String rid;
    private String restaurant_name;
    private String meal_name;
    private String menu_id;

    private HashMap map;
    private JSONObject res;
    private MediaPlayer mp;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    public NotificationHelper(Context context, Activity activity){
        Log.d("notif setup","context: "+context+" activity: "+activity);
        this.context = context;
        this.activity = activity;

        preferenceHelper = new PreferenceHelper(this.context);
        parseContent = new ParseContent(this.activity);

        map = new HashMap();

        //get the user's current location and map the coordinates
        String longitude = preferenceHelper.getLongitude();
        map.put(AndyConstants.Params.LONGITUDE, String.valueOf(longitude));
        String latitude = preferenceHelper.getLatitude();
        map.put(AndyConstants.Params.LATITUDE, String.valueOf(latitude));

        //update the location and get the menu info
        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEUPDATELOCATION);

                    map.put(AndyConstants.Params.UID, preferenceHelper.getUID());
                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=parseContent.getErrorMessage(response);
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("postresp", result);
                onTaskCompleted(result);
            }
        }.execute();


    }

    private void onTaskCompleted(String result){
        Log.d("response", result);

//        Log.d("data", String.valueOf(parseContent.getData(result)));
//        Log.d("obj", String.valueOf(parseContent.getDataObj(result)));

        if(parseContent.getDataObj(result) == null){//if there are no results just return control to the calling activity
            Log.d("inside","ok");
            //do something?

        }
        else {//if there are results, show them
            Log.d("else","else");
            try {
                res = parseContent.getDataObj(result);
                rid = res.getString(AndyConstants.Params.RID);
                restaurant_name = String.valueOf(Html.fromHtml(res.getString(AndyConstants.Params.RESTAURANT_NAME)));
                meal_name = String.valueOf(Html.fromHtml(res.getString(AndyConstants.Params.MEAL_NAME)));
                menu_id = res.getString(AndyConstants.Params.MENU_ID);

                createNotificationModal();
            } catch (JSONException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }


//            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
    }


    public void createNotificationModal()throws IOException, JSONException {

        //sound
        mp = MediaPlayer.create(context, R.raw.triangle_dinner_bell);
//        mp.setLooping(true); //uncomment for continuous sound
        mp.start();

        notificationDialogBuilder = new AlertDialog.Builder(this.context).create();
        // Get the layout inflater
        LayoutInflater inflater = LayoutInflater.from(this.context);

        View dialogView = inflater.inflate(R.layout.custom_notification_dialog, null);

        TextView message = dialogView.findViewById(R.id.message);
        message.setText(restaurant_name + " " + meal_name + " Menu is available right now.");

        Button viewButton = dialogView.findViewById(R.id.viewButton);
        Button dismissButton = dialogView.findViewById(R.id.dismissButton);
        Button directionsButton = dialogView.findViewById(R.id.directionsButton);


        dismissButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                mp.stop();
                mp.release();
                mp = null;

                notificationDialogBuilder.dismiss();
            }
        });

        viewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, meal_name+" Menu", Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, DetailsMenuActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putString(AndyConstants.Params.MENU_ID,menu_id);
                intent.putExtras(mBundle);
                preferenceHelper.putActivityChanged(true);
                startActivity(context, intent, mBundle);

                mp.stop();
                mp.release();
                mp = null;

                notificationDialogBuilder.dismiss();
            }
        });

        directionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Toast.makeText(context, "Directions to "+restaurant_name, Toast.LENGTH_SHORT).show();
                Intent intent = new Intent(context, DirectionsActivity.class);
                Bundle mBundle = new Bundle();
                mBundle.putString(AndyConstants.Params.RID,rid);
                intent.putExtras(mBundle);
                preferenceHelper.putActivityChanged(true);
                startActivity(context, intent, mBundle);

                mp.stop();
                mp.release();
                mp = null;

                notificationDialogBuilder.dismiss();
            }
        });

        notificationDialogBuilder.setView(dialogView);
        notificationDialogBuilder.show();

    }

}
