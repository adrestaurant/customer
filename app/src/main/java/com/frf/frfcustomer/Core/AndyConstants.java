package com.frf.frfcustomer.Core;

public class AndyConstants {

    // web service url constants
    public class ServiceType {
//        Base URL
//        public static final String BASE_URL = "http://172.30.20.81/ad-api-full-test/customer/";
//        public static final String BASE_URL = "http://10.0.2.2/ad-api-full-test/customer/";
        public static final String BASE_URL = "https://findrestfood.000webhostapp.com/customer/";

//        base for image folder THIS IS ONLY FOR TESTING AND WILL BE REMOVED ONCE WE MOVE TO THE FINAL SERVER
//        public static final String IMAGES = "http://172.30.20.81/ad-api-full-test/";
//        public static final String IMAGES = "http://10.0.2.2/ad-api-full-test/";

                  public static final String IMAGES = "https://findrestfood.000webhostapp.com/";

//        Login link
        public static final String LOGIN = BASE_URL + "login";//logs the user in

//        Sign Up links
        public static final String SIGNUPINDEX =  BASE_URL + "signup/index";//gets the values for the diet types expandable list
        public static final String SIGNUPACTION =  BASE_URL + "signup/signup";//creates a new customer user

//      Restaurant links
        public static final String RESTAURANTINDEX = BASE_URL + "restaurants/index";//gets all the filter values to populate the filter screen
        public static final String RESTAURANTFILTER = BASE_URL + "restaurants/filter";//gets all restaurants that match the desired filters (used in listing screen)
        public static final String RESTAURANTDETAILS = BASE_URL + "restaurants/details";//get all the details for the restaurant details screen

        //      Ratings links
        public static final String RATINGSINDEX = BASE_URL + "ratings/index";//get all ratings for a restaurant
        public static final String RATINGSGETONE = BASE_URL + "ratings/getone";//gets a user's review based on rid and uid
        public static final String RATINGSINSERT = BASE_URL + "ratings/insert";//used to create or update a review

        //      Menus links
        public static final String MENUSINDEX = BASE_URL + "menus/index";//gets all the filter values to populate the filter screen
        public static final String MENUSFILTER = BASE_URL + "menus/filter";//gets all menus that match the desired filters
        public static final String MENUSDETAILS= BASE_URL + "menus/details";//get all the details for the menu details screen also used to show results of the dishes filter activity

        //      Dishes links
        public static final String DISHESINDEX = BASE_URL + "dishes/index";//gets all the filter values to populate the filter screen
        public static final String DISHESDETAILS= BASE_URL + "dishes/details";//get all the details for the dish details screen

        //      Profile links
        public static final String PROFILEINDEX = BASE_URL + "profile/index";//gets all the user's data and the info needed for the diet types expandable list
        public static final String PROFILEUPDATE = BASE_URL + "profile/update";//updates the user's data
        public static final String PROFILEUPDATELOCATION = BASE_URL + "profile/updatelocation";//updates the user's location and gets the info of the closest
                                                                                               // restaurant that's no more than 15 meters away, for the notification

    }
    // webservice key constants
    public class Params {

        public static final String USERNAME = "username";
        public static final String PASSWORD = "password";
        public static final String PASSWORDCONFIRM = "passwordConfirm";
        public static final String EMAIL = "email";
        public static final String F_NAME = "f_name";
        public static final String L_NAME = "l_name";
        public static final String LONGITUDE = "longitude";
        public static final String LATITUDE = "latitude";
        public static  final String UID = "uid";
        public static  final String DT_ID = "dt_id";
        public static  final String PID = "pid";
        public static  final String VID = "vid";
        public static final String DT_NAME = "name";
        public static final String PARISH_NAME = "name";
        public static final String VILLAGE_NAME = "name";
        public static final String CUISINE_NAME = "name";

        public static final String RESTAURANT_NAME = "restaurant_name";
        public static final String CUISINE = "cuisine";
        public static final String OPEN_TIME = "open_time";
        public static final String CLOSE_TIME = "close_time";
        public static final String RID = "rid";
        public static final String STREET = "street";
        public static final String VILLAGE = "village";
        public static final String PARISH = "parish";
        public static final String DISTANCE = "distance";
        public static final String DESCRIPTION = "description";
        public static final String CONTACT_PERSON_F_NAME = "contact_person_f_name";
        public static final String CONTACT_PERSON_L_NAME = "contact_person_l_name";
        public static final String TELEPHONE_NUMBER = "telephone_number";
        public static final String NEARME = "nearme";
        public static final String DIET_PREFERENCES = "diet_preferences";
        public static final String USER_DIET_PREFERENCES = "user_diet_preferences";
        public static final String MIN_PRICE = "min_price";
        public static final String MAX_PRICE = "max_price";
        public static final String PARISHES = "parishes";
        public static final String VILLAGES = "villages";
        public static final String CU_ID = "cu_id";
        public static final String DIET_TYPE = "diet_type";
        public static final String START_TIME = "start_time";
        public static final String END_TIME = "end_time";
        public static final String IMAGE_URL = "image_url";
        public static final String MEAL_NAME = "meal_name";
        public static final String MENU_ID = "menu_id";
        public static final String RATING = "rating";
        public static final String COMMENT = "comment";
        public static final String CREATED_AT   = "created_at";
        public static final String UPDATED_AT   = "updated_at";
        public static final String RATINGS = "ratings";
        public static final String FOOD_CATEGORIES = "food_categories";
        public static final String MEAL = "meal";
        public static final String D_ID = "d_id";
        public static final String DISH_NAME = "name";
        public static final String FOOD_CATEGORY = "food_category";
        public static final String PRICE = "price";
        public static final String DISH_IMAGE = "dish_image";
        public static final String DB_FC_NAME = "name";
        public static final String MEAL_ID = "meal_id";
        public static final String FC_ID = "fc_id";
        public static final String MEALS = "meals";
        public static final String DB_MEAL_NAME = "name";

        public static final String API_KEY = "AIzaSyBMdY50KkYdWjhRuesVCnoeaPZjM3zPSmo";
    }

}
