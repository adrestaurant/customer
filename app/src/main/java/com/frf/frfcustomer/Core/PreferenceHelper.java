package com.frf.frfcustomer.Core;

import android.content.Context;
import android.content.SharedPreferences;

public class PreferenceHelper {

    public static final String MyPREFERENCES = "MyPrefs" ;
    private final String USERNAME = "username";
    private final String PASSWORD = "password";
    private final String USERPASS = "userpass";
    private final String UID = "uid";
    private final String MINPRICE ="min_price";
    private final String MAXPRICE = "max_price";
    private final String LOGINSKIPPED = "login_skipped";
    private final String LONGITUDE = "longitude";
    private final String LATITUDE = "latitude";
    private final String ACTIVITYCHANGED  = "activity_changed";
    private final String FROMLOGIN  = "from_login";//will determine if listing restaurant was access directly from login
    private SharedPreferences sharedpreferences;
    private Context context;

    public PreferenceHelper(Context context) {
        sharedpreferences = context.getSharedPreferences(MyPREFERENCES, Context.MODE_PRIVATE);
        this.context = context;
    }

    public void putUsername(String uname) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(USERNAME, uname);
         edit.commit();
    }
    public String getUsername() {
        return sharedpreferences.getString(USERNAME, "").trim();
    }

    public void putPassword(String pword) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(PASSWORD, pword);
         edit.commit();
    }
    public String getPassword() {
        return sharedpreferences.getString(PASSWORD, "").trim();
    }

    public void putUserPass(String up) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(USERPASS, up);
         edit.commit();
    }
    public String getUserPass() {
        return sharedpreferences.getString(USERPASS, "").trim();
    }

    public void putUID(String uid) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(UID, uid);
        edit.commit();
    }
    public String getUID() {
        return sharedpreferences.getString(UID, "").trim();
    }

    public void putMinPrice(String price) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(MINPRICE, price);
        edit.commit();
    }
    public String getMinPrice() {
        return sharedpreferences.getString(MINPRICE, "").trim();
    }

    public void putMaxPrice(String price) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(MAXPRICE, price);
        edit.commit();
    }

    public String getMaxPrice() {
        return sharedpreferences.getString(MAXPRICE, "").trim();
    }

    public String getLoginSkipped() {
        return sharedpreferences.getString(LOGINSKIPPED, "").trim();
    }

    public void putLoginSkipped(String val) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(LOGINSKIPPED, val);
        edit.commit();
    }


    public String getLongitude() {
        return sharedpreferences.getString(LONGITUDE, "").trim();
    }

    public void putLongitude (String value) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(LONGITUDE, value);
        edit.commit();
    }

    public String getLatitude() {
        return sharedpreferences.getString(LATITUDE, "").trim();
    }

    public void putLatitude (String value) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putString(LATITUDE, value);
        edit.commit();
    }

    public Boolean getActivityChanged() {
        return sharedpreferences.getBoolean(ACTIVITYCHANGED, false);
    }

    public void putActivityChanged (boolean value) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putBoolean(ACTIVITYCHANGED, value);
        edit.commit();
    }
    public Boolean getFromLogin() {
        return sharedpreferences.getBoolean(FROMLOGIN, false);
    }

    public void putFromLogin (boolean value) {
        SharedPreferences.Editor edit = sharedpreferences.edit();
        edit.putBoolean(FROMLOGIN, value);
        edit.commit();
    }


    public void clearPrefs() {
        SharedPreferences.Editor editor = sharedpreferences.edit();
        editor.clear();
        editor.commit();
    }

}
