package com.frf.frfcustomer;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;
import com.frf.frfcustomer.ExpandableAdapters.DietPrefsExpandableListAdapter;
import com.frf.frfcustomer.ExpandableModels.DataItem;
import com.frf.frfcustomer.ExpandableModels.SubCategoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static com.frf.frfcustomer.Core.MD5Password.getMD5EncryptedValue;

public class SignupActivity extends AppCompatActivity implements IGPSActivity {

    //for expandable list (diet preferences)
    private ExpandableListView dietTypeList;

    private ArrayList<DataItem> dietPrefsArCategory;
    private ArrayList<SubCategoryItem> dietPrefsArSubCategory;
    private ArrayList<ArrayList<SubCategoryItem>> dietPrefsArSubCategoryFinal;

    private ArrayList<HashMap<String, String>> dietPrefsParentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> dietPrefsChildItems;
    private DietPrefsExpandableListAdapter dietPrefsExpandableListAdapter;


    //for form
    private EditText f_name, l_name, username, email, password, passwordConfirm;
    private Button signupButton;
    private TextView loginLink;
    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;
    private final int RegTask = 1;
    private JSONArray dietPrefs;

    int childHeight;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_signup);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        gps = new GPS(this);

        f_name = findViewById(R.id.f_name);
        l_name = findViewById(R.id.l_name);
        username = findViewById(R.id.username);
        email = findViewById(R.id.email);
        password = findViewById(R.id.password);
        passwordConfirm = findViewById(R.id.passwordConfirm);

        signupButton = findViewById(R.id.signupButton);
        loginLink = findViewById(R.id.loginLink);


        signupButton.setOnClickListener(new View.OnClickListener() {//only use for testing purposes, once I sort it out, call signup function
            @Override
            public void onClick(View v) {
                try {
                    signup();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        loginLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
                SignupActivity.this.finish();
            }
        });

        new FetchDietPrefsAsyncTask().execute();
    }


//for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }



    /**
     * Fetches the list of diet preferences from the server
     */
    private class FetchDietPrefsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(SignupActivity.this)) {
                Toast.makeText(SignupActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(SignupActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.SIGNUPINDEX);
                response = req.prepare(HttpRequest.Method.POST).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("dietprefsjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }


    private void setupReferences(String result) {

        dietTypeList = findViewById(R.id.dietTypeList);

        dietPrefsArCategory = new ArrayList<>();
        dietPrefsArSubCategory = new ArrayList<>();
        dietPrefsParentItems = new ArrayList<>();
        dietPrefsChildItems = new ArrayList<>();

        Log.d("received refs", result);
        if (parseContent.isSuccess(result)) {
            dietPrefs = parseContent.getData(result);
        }else {//if something goes wrong with the retrieval, put them back at the login screen with an error toast
            Toast.makeText(SignupActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }

        DataItem dataItem = new DataItem();
        dataItem.setCategoryId("diet_preferences");
        dataItem.setCategoryName("Diet Preferences");

        dietPrefsArSubCategory = new ArrayList<>();

        for(int i = 0; i < dietPrefs.length(); i++) {
            JSONObject dietPref = null;
            try {
                dietPref = dietPrefs.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("diet_preferences");
                subCategoryItem.setSubId(String.valueOf(dietPref.getInt(AndyConstants.Params.DT_ID)));
                subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                subCategoryItem.setSubCategoryName(dietPref.getString(AndyConstants.Params.DT_NAME));
                dietPrefsArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        dataItem.setSubCategory(dietPrefsArSubCategory);
        dietPrefsArCategory.add(dataItem);

        Log.d("TAG", "setupReferences: "+dietPrefsArCategory.size());

        for(DataItem data : dietPrefsArCategory){
//                        Log.i("Item id",item.id);
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            dietPrefsChildItems.add(childArrayList);
            dietPrefsParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = dietPrefsParentItems;
        ExpandableConstantManager.childItems = dietPrefsChildItems;

        dietPrefsExpandableListAdapter = new DietPrefsExpandableListAdapter(this,dietPrefsParentItems,dietPrefsChildItems,false);
        dietTypeList.setAdapter(dietPrefsExpandableListAdapter);

        Log.d("TAG", "parent: "+dietPrefsParentItems);
        Log.d("TAG", "children: "+dietPrefsChildItems);

        dietTypeList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("dietType children","count= "+dietTypeList.getChildCount());
                for (int i = 0; i < dietTypeList.getChildCount(); i++) {
                    childHeight = dietTypeList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + dietTypeList.getDividerHeight();
                }

                Log.d("count", String.valueOf(dietPrefsExpandableListAdapter.getChildrenCount(groupPosition)));
                dietTypeList.getLayoutParams().height = (height) * (dietPrefsExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        dietTypeList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                dietTypeList.getLayoutParams().height = childHeight;
            }
        });
    }


    public void signup()throws IOException, JSONException {
        if (!AndyUtils.isNetworkAvailable(SignupActivity.this)) {
            Toast.makeText(SignupActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(SignupActivity.this);
        final HashMap<String, String> map = new HashMap<>();

        map.put(AndyConstants.Params.F_NAME, f_name.getText().toString());
        map.put(AndyConstants.Params.L_NAME, l_name.getText().toString());
        map.put(AndyConstants.Params.USERNAME, username.getText().toString());
        map.put(AndyConstants.Params.EMAIL, email.getText().toString());

        String encryptedPassword = getMD5EncryptedValue(password.getText().toString());
        map.put(AndyConstants.Params.PASSWORD, encryptedPassword);

        String encryptedPasswordConfirm = getMD5EncryptedValue(passwordConfirm.getText().toString());
        map.put(AndyConstants.Params.PASSWORDCONFIRM, encryptedPasswordConfirm);

        for (int i = 0; i < DietPrefsExpandableListAdapter.parentItems.size(); i++ ) {
            String parentID = DietPrefsExpandableListAdapter.parentItems.get(i).get(ExpandableConstantManager.Parameter.CATEGORY_ID);//the name of the category in a form that makes sense for the php

            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < DietPrefsExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }

            }
            map.put(parentID, String.valueOf(children));
        }



        //get the user's current location and map the coordinates
        String longitude = preferenceHelper.getLongitude();
        map.put(AndyConstants.Params.LONGITUDE, String.valueOf(longitude));
        String latitude = preferenceHelper.getLatitude();
        map.put(AndyConstants.Params.LATITUDE, String.valueOf(latitude));

        Log.d("request: ", String.valueOf(map));


        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.SIGNUPACTION);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                onTaskCompleted(result, RegTask);
            }
        }.execute();
    }
    private void onTaskCompleted(String response,int task) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case RegTask:

                if (parseContent.isSuccess(response)) {
                    Toast.makeText(SignupActivity.this, "Registered Successfully! Please Login.", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(SignupActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    preferenceHelper.putActivityChanged(true);
                    startActivity(intent);
                    this.finish();
                }else {
                    Toast.makeText(SignupActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                }
        }
    }

}
