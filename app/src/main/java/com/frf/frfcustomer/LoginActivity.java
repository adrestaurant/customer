package com.frf.frfcustomer;


import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;

import org.json.JSONException;

import java.io.IOException;
import java.util.HashMap;

import static com.frf.frfcustomer.Core.MD5Password.getMD5EncryptedValue;

public class LoginActivity extends AppCompatActivity implements IGPSActivity {

    private EditText username, password;
    private Button loginButton;
    private TextView signupLink;
    private ParseContent parseContent;
    private final int LoginTask = 1;
    private PreferenceHelper preferenceHelper;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        gps = new GPS(this);

        username = findViewById(R.id.username);
        password = findViewById(R.id.password);

        loginButton = findViewById(R.id.loginButton);
        signupLink = findViewById(R.id.signupLink);

        signupLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(LoginActivity.this, SignupActivity.class);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });

        loginButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    login();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    private void login() throws IOException, JSONException {

        if (!AndyUtils.isNetworkAvailable(LoginActivity.this)) {
            Toast.makeText(LoginActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }

        AndyUtils.showSimpleProgressDialog(LoginActivity.this);

        final HashMap<String, String> map = new HashMap<>();

        map.put(AndyConstants.Params.USERNAME, username.getText().toString());

        String encryptedPassword = getMD5EncryptedValue(password.getText().toString());
        map.put(AndyConstants.Params.PASSWORD, encryptedPassword);



        //get the user's current location and map the coordinates
        String longitude = preferenceHelper.getLongitude();
        map.put(AndyConstants.Params.LONGITUDE, String.valueOf(longitude));
        String latitude = preferenceHelper.getLatitude();
        map.put(AndyConstants.Params.LATITUDE, String.valueOf(latitude));

        
        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.LOGIN);
                    response = req.prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("postresp", result);
                onTaskCompleted(result,LoginTask);
            }
        }.execute();
    }

    private void onTaskCompleted(String response,int task) {
        Log.d("responsejson", response);
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
        switch (task) {
            case LoginTask:
                if (parseContent.isSuccess(response)) {
                    parseContent.saveInfo(response);
                    preferenceHelper.putLoginSkipped("false");

                    Toast.makeText(LoginActivity.this, "Login Successfully!", Toast.LENGTH_SHORT).show();

                    Intent intent = new Intent(LoginActivity.this, ListingRestaurantActivity.class);
                    intent.putExtra(AndyConstants.Params.UID,preferenceHelper.getUID());
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    preferenceHelper.putFromLogin(true);
                    startActivity(intent);
                    this.finish();
                }else {
                    Toast.makeText(LoginActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
                }
        }
    }

}
