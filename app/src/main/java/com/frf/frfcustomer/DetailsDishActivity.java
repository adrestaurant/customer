package com.frf.frfcustomer;

import android.content.Intent;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;
import com.squareup.picasso.Picasso;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class DetailsDishActivity extends AppCompatActivity implements IGPSActivity {

    TextView restaurant_name, name, price, description, menu, food_category, cuisine, diet_type;
    ImageView image;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private String d_id;

    private HashMap<String, String> map;
    private JSONObject details;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_dish);

        Intent intent = getIntent();
        d_id =  intent.getStringExtra(AndyConstants.Params.D_ID);
        Log.d("det dish d_id",d_id);

        gps = new GPS(this);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        restaurant_name = findViewById(R.id.restaurant_name);
        name = findViewById(R.id.name);
        image = findViewById(R.id.image);
        price = findViewById(R.id.price);
        description = findViewById(R.id.description);
        menu = findViewById(R.id.menu);
        food_category = findViewById(R.id.food_category);
        cuisine = findViewById(R.id.cuisine);
        diet_type = findViewById(R.id.diet_type);

        map = new HashMap<>();


        new FetchDetailsAsyncTask().execute();
    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }





    /**
     * Fetches the details (based on d_id)
     */
    private class FetchDetailsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(DetailsDishActivity.this)) {
                Toast.makeText(DetailsDishActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(DetailsDishActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.DISHESDETAILS);

                map.put(AndyConstants.Params.D_ID, d_id);

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("result", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            try {
                details = parseContent.getDataObj(result);//get the details from the results
                restaurant_name.setText(Html.fromHtml(details.getString(AndyConstants.Params.RESTAURANT_NAME)));//set the restaurant name
                name.setText(Html.fromHtml(details.getString("dish_name")));//set the dish name
                //get the image
                Picasso.get().load(Uri.parse(AndyConstants.ServiceType.IMAGES + details.getString("dish_image")))
                        .resize(600, 300)
                        .placeholder(R.drawable.image_placeholder)
                        .error(R.drawable.image_placeholder_error)
                        .into(image);

                price.setText(details.getString(AndyConstants.Params.PRICE));//set the price
                description.setText(details.getString(AndyConstants.Params.DESCRIPTION));//set the description
                menu.setText(details.getString("menu_name"));//set the menu name
                food_category.setText(details.getString(AndyConstants.Params.FOOD_CATEGORY));//set the food category
                cuisine.setText(details.getString(AndyConstants.Params.CUISINE));//set the cuisine
                diet_type.setText(details.getString(AndyConstants.Params.DIET_TYPE));//set the diet type

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }

}
