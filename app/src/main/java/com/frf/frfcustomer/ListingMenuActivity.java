package com.frf.frfcustomer;

import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfcustomer.CardAdapters.MenuCardAdapter;
import com.frf.frfcustomer.CardModels.MenuModel;
import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListingMenuActivity extends AppCompatActivity implements IGPSActivity {

    private RecyclerView recyclerView;
    private Button filterButton;
    private ArrayList<MenuModel> menuModelArrayList;
    private MenuCardAdapter adapter;
    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private String rid;

    private JSONArray menus;

    private HashMap<String, String> map;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_menus);


        gps = new GPS(this);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        recyclerView = findViewById(R.id.recyclerView);

        filterButton = findViewById(R.id.filterButton);

        map = new HashMap<>();

        //add the filter params to the hashmap that stores the request data
        Intent intent = getIntent();
        rid = intent.getStringExtra(AndyConstants.Params.RID);
        map.put(AndyConstants.Params.RID, rid);//get the rid

        map.put(AndyConstants.Params.UID,intent.getStringExtra(AndyConstants.Params.UID));//get UID, would be set if they came from the restaurant details screen


        //need to get filter values from the intent and map them to the
        map.put(AndyConstants.Params.CUISINE, intent.getStringExtra(AndyConstants.Params.CUISINE));
        map.put(AndyConstants.Params.DIET_PREFERENCES, intent.getStringExtra(AndyConstants.Params.DIET_PREFERENCES));
        map.put(AndyConstants.Params.MIN_PRICE, intent.getStringExtra(AndyConstants.Params.MIN_PRICE));
        map.put(AndyConstants.Params.MAX_PRICE, intent.getStringExtra(AndyConstants.Params.MAX_PRICE));
        map.put(AndyConstants.Params.FOOD_CATEGORIES, intent.getStringExtra(AndyConstants.Params.FOOD_CATEGORIES));
        map.put(AndyConstants.Params.START_TIME, intent.getStringExtra(AndyConstants.Params.START_TIME));
        map.put(AndyConstants.Params.END_TIME, intent.getStringExtra(AndyConstants.Params.END_TIME));
        map.put(AndyConstants.Params.MEAL, intent.getStringExtra(AndyConstants.Params.MEAL));

        Log.d("filters", String.valueOf(map));

        new FetchMenusAsyncTask().execute();



        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Toast.makeText(ListingMenuActivity.this, menuModelArrayList.get(position).getMealName()+" Menu", Toast.LENGTH_SHORT).show();
                String menu_id = menuModelArrayList.get(position).getMenuID();
                Intent intent = new Intent(ListingMenuActivity.this, DetailsMenuActivity.class);
                intent.putExtra(AndyConstants.Params.MENU_ID,menu_id);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListingMenuActivity.this, FilterMenuActivity.class);
                intent.putExtra(AndyConstants.Params.RID,rid);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });

    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }


    /**
     * Fetches the list of menus from the server (based on rid and filters)
     */
    private class FetchMenusAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ListingMenuActivity.this)) {
                Toast.makeText(ListingMenuActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ListingMenuActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.MENUSFILTER);

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("menusjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            if(parseContent.getData(result).length() == 0){//if there are no results show the text view
                showNoResults();
            }
            else {//if there are results, show them
                populateList(result);
            }
        }

    }

    private void showNoResults(){
        TextView noResultsTV = findViewById(R.id.noResultsTV);
        noResultsTV.setVisibility(View.VISIBLE);
    }

    private ArrayList<MenuModel> populateList(String result){

        menuModelArrayList = new ArrayList<MenuModel>();

        Log.d("received rest info", result);
        if (parseContent.isSuccess(result)) {
            menus = parseContent.getData(result);

            for(int i = 0; i < menus.length(); i++) {
                JSONObject menu = null;
                try {
                    menu = menus.getJSONObject(i);
                    MenuModel menuModel = new MenuModel();

                    menuModel.setMealName(menu.getString(AndyConstants.Params.MEAL_NAME));
                    menuModel.setDietType(menu.getString(AndyConstants.Params.DIET_TYPE));
                    menuModel.setCuisine(menu.getString(AndyConstants.Params.CUISINE));
                    menuModel.setStartTime(menu.getString(AndyConstants.Params.START_TIME));
                    menuModel.setEndTime(menu.getString(AndyConstants.Params.END_TIME));
                    menuModel.setMenuID(menu.getString(AndyConstants.Params.MENU_ID));
                    menuModel.setImageUrl(menu.getString(AndyConstants.Params.IMAGE_URL));

                    menuModelArrayList.add(menuModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.d("number of menus", menuModelArrayList.size() + "");

            adapter = new MenuCardAdapter(this,menuModelArrayList);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        }else {//if something goes wrong with the retrieval, put them back at the details screen with an error toast
            Toast.makeText(ListingMenuActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ListingMenuActivity.this, ListingRestaurantActivity.class);
            intent.putExtra(AndyConstants.Params.RID,rid);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }


        return menuModelArrayList;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
