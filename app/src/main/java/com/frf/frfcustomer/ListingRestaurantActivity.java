package com.frf.frfcustomer;


import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.util.Log;
import android.view.GestureDetector;
import android.view.MotionEvent;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfcustomer.CardAdapters.RestaurantCardAdapter;
import com.frf.frfcustomer.CardModels.RestaurantModel;
import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.HashMap;

public class ListingRestaurantActivity extends AppCompatActivity implements IGPSActivity {

    private RecyclerView recyclerView;
    private Button filterButton, profileButton, logoutButton;
    private ArrayList<RestaurantModel> restaurantModelArrayList;
    private RestaurantCardAdapter adapter;
    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private JSONArray restaurants;

    private HashMap<String, String> map;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_listing_restaurants);

        gps = new GPS(this);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        recyclerView = findViewById(R.id.recyclerView);

        filterButton = findViewById(R.id.filterButton);
        profileButton = findViewById(R.id.profileButton);
        logoutButton = findViewById(R.id.logoutButton);

        map = new HashMap<>();

        //add the filter params to the hashmap that stores the request data
        Intent intent = getIntent();

        map.put(AndyConstants.Params.NEARME, intent.getStringExtra(AndyConstants.Params.NEARME));
        map.put(AndyConstants.Params.CUISINE, intent.getStringExtra(AndyConstants.Params.CUISINE));
        map.put(AndyConstants.Params.DIET_PREFERENCES, intent.getStringExtra(AndyConstants.Params.DIET_PREFERENCES));
        map.put(AndyConstants.Params.OPEN_TIME, intent.getStringExtra(AndyConstants.Params.OPEN_TIME));
        map.put(AndyConstants.Params.CLOSE_TIME, intent.getStringExtra(AndyConstants.Params.CLOSE_TIME));
        map.put(AndyConstants.Params.MIN_PRICE, intent.getStringExtra(AndyConstants.Params.MIN_PRICE));
        map.put(AndyConstants.Params.MAX_PRICE, intent.getStringExtra(AndyConstants.Params.MAX_PRICE));
        map.put(AndyConstants.Params.PARISHES, intent.getStringExtra(AndyConstants.Params.PARISHES));
        map.put(AndyConstants.Params.VILLAGES, intent.getStringExtra(AndyConstants.Params.VILLAGES));

        if(preferenceHelper.getLoginSkipped().equals("true")){//if they came here straight from the splash screen show a welcome toast
            Toast.makeText(ListingRestaurantActivity.this, "Welcome back "+preferenceHelper.getUsername()+"!", Toast.LENGTH_SHORT).show();
            preferenceHelper.putLoginSkipped("false");
        }

        if(intent.getExtras().containsKey(AndyConstants.Params.UID)){
            //get the user's diet preferences
            new getDietPrefsAsync().execute();
        }


        new FetchRestaurantsAsyncTask().execute();



        recyclerView.addOnItemTouchListener(new RecyclerTouchListener(getApplicationContext(), recyclerView, new ClickListener() {

            @Override
            public void onClick(View view, int position) {
                Toast.makeText(ListingRestaurantActivity.this, restaurantModelArrayList.get(position).getRestaurantName(), Toast.LENGTH_SHORT).show();
                String rid = restaurantModelArrayList.get(position).getRID();
                Intent intent = new Intent(ListingRestaurantActivity.this, DetailsRestaurantActivity.class);
                intent.putExtra(AndyConstants.Params.RID,rid);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
            @Override
            public void onLongClick(View view, int position) {

            }
        }));

        filterButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListingRestaurantActivity.this, FilterRestaurantActivity.class);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });

        profileButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(ListingRestaurantActivity.this, ProfileActivity.class);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });

        logoutButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                preferenceHelper.clearPrefs();
                Intent intent = new Intent(ListingRestaurantActivity.this, LoginActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
                ListingRestaurantActivity.this.finish();
            }
        });

    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }



    /**
     * Fetches the user's diet prefs from the server
     */
    private class getDietPrefsAsync extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ListingRestaurantActivity.this)) {
                Toast.makeText(ListingRestaurantActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ListingRestaurantActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEINDEX);
                HashMap dietPrefsMap = new HashMap();
                dietPrefsMap.put(AndyConstants.Params.UID,preferenceHelper.getUID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(dietPrefsMap).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            Log.d("response doinbcgrnd",response);
            afterRetrieve(response);
            return response;
        }

        protected void afterRetrieve(String result) {
            Log.d("dietprefsjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            JSONObject res = parseContent.getDataObj(result);
            try {
                JSONArray prefs = res.getJSONArray(AndyConstants.Params.USER_DIET_PREFERENCES);
                String dietPrefs = "[";
                Log.d("dietPrefs inside b4", dietPrefs);
                Log.d("res length", String.valueOf(res.length()));
                for(int i=0; i < prefs.length(); i++) {

                    dietPrefs += prefs.getJSONObject(i).getString(AndyConstants.Params.DT_ID);
                    dietPrefs += ",";
                }
                dietPrefs = dietPrefs.substring(0, dietPrefs.length() - 1);//remove last ,
                dietPrefs += "]";
                Log.d("dietPrefs inside after", dietPrefs);
                map.put(AndyConstants.Params.DIET_PREFERENCES,dietPrefs);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

    }


    /**
     * Fetches the list of restaurants from the server (based on diet preferences)
     */
    private class FetchRestaurantsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ListingRestaurantActivity.this)) {
                Toast.makeText(ListingRestaurantActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ListingRestaurantActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.RESTAURANTFILTER);

                map.put(AndyConstants.Params.UID, preferenceHelper.getUID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("restaurantjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

            if(parseContent.getData(result).length() == 0){//if there are no results show the text view
                showNoResults();
            }
            else {//if there are results, show them
                populateList(result);
            }
        }

    }

    private void showNoResults(){
        TextView noResultsTV = findViewById(R.id.noResultsTV);
        noResultsTV.setVisibility(View.VISIBLE);
    }


    private ArrayList<RestaurantModel> populateList(String result){

        restaurantModelArrayList = new ArrayList<RestaurantModel>();

        Log.d("received rest info", result);
        if (parseContent.isSuccess(result)) {
            restaurants = parseContent.getData(result);

            for(int i = 0; i < restaurants.length(); i++) {
                JSONObject restaurant = null;
                try {
                    restaurant = restaurants.getJSONObject(i);
                    RestaurantModel restaurantModel = new RestaurantModel();

                    restaurantModel.setRestaurantName(restaurant.getString(AndyConstants.Params.RESTAURANT_NAME));
                    restaurantModel.setCuisine(restaurant.getString(AndyConstants.Params.CUISINE));
                    restaurantModel.setOpenTime(restaurant.getString(AndyConstants.Params.OPEN_TIME));
                    restaurantModel.setCloseTime(restaurant.getString(AndyConstants.Params.CLOSE_TIME));
                    restaurantModel.setRID(restaurant.getString(AndyConstants.Params.RID));
                    restaurantModel.setDistance(restaurant.getString(AndyConstants.Params.DISTANCE));

                    String address = restaurant.getString(AndyConstants.Params.STREET)+ ", " +
                            restaurant.getString(AndyConstants.Params.VILLAGE) + ", "+
                            restaurant.getString(AndyConstants.Params.PARISH);

                    restaurantModel.setAddress(address);

                    restaurantModelArrayList.add(restaurantModel);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }

            Log.d("number of restaurants", restaurantModelArrayList.size() + "");

            adapter = new RestaurantCardAdapter(this,restaurantModelArrayList);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(getApplicationContext(), LinearLayoutManager.VERTICAL, false));

        }else {//if something goes wrong with the retrieval, put them back at the login screen with an error toast
            Toast.makeText(ListingRestaurantActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ListingRestaurantActivity.this, LoginActivity.class);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }


        return restaurantModelArrayList;
    }

    public interface ClickListener {
        void onClick(View view, int position);

        void onLongClick(View view, int position);
    }

    static class RecyclerTouchListener implements RecyclerView.OnItemTouchListener {

        private GestureDetector gestureDetector;
        private ClickListener clickListener;

        public RecyclerTouchListener(Context context, final RecyclerView recyclerView, final ClickListener clickListener) {
            this.clickListener = clickListener;
            gestureDetector = new GestureDetector(context, new GestureDetector.SimpleOnGestureListener() {
                @Override
                public boolean onSingleTapUp(MotionEvent e) {
                    return true;
                }

                @Override
                public void onLongPress(MotionEvent e) {
                    View child = recyclerView.findChildViewUnder(e.getX(), e.getY());
                    if (child != null && clickListener != null) {
                        clickListener.onLongClick(child, recyclerView.getChildPosition(child));
                    }
                }
            });
        }

        @Override
        public boolean onInterceptTouchEvent(RecyclerView rv, MotionEvent e) {

            View child = rv.findChildViewUnder(e.getX(), e.getY());
            if (child != null && clickListener != null && gestureDetector.onTouchEvent(e)) {
                clickListener.onClick(child, rv.getChildPosition(child));
            }
            return false;
        }

        @Override
        public void onTouchEvent(RecyclerView rv, MotionEvent e) {
        }

        @Override
        public void onRequestDisallowInterceptTouchEvent(boolean disallowIntercept) {

        }
    }

}
