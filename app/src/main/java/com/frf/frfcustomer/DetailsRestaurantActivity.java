package com.frf.frfcustomer;

import android.app.Activity;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.HashMap;

public class DetailsRestaurantActivity extends AppCompatActivity  implements IGPSActivity {

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private TextView restaurant_name, description, cuisine, address, open_time, close_time, distance, contact_person_name, email, telephone_number;
    private Button menuButton, myReviewButton, reviewsButton, directionsButton;

    private String rid;

    private JSONArray details;

    //for location stuff
    private GPS gps;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_details_restaurant);

        gps = new GPS(this);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        Intent intent = getIntent();
        rid = intent.getStringExtra(AndyConstants.Params.RID);


        new FetchDetailsAsyncTask().execute();

        restaurant_name = findViewById(R.id.restaurant_name);
        description = findViewById(R.id.description);
        cuisine = findViewById(R.id.cuisine);
        address = findViewById(R.id.address);
        open_time = findViewById(R.id.open_time);
        close_time = findViewById(R.id.close_time);
        distance = findViewById(R.id.distance);
        contact_person_name = findViewById(R.id.contact_person_name);
        email = findViewById(R.id.email);
        telephone_number = findViewById(R.id.telephone_number);

        menuButton = findViewById(R.id.menuButton);
        myReviewButton = findViewById(R.id.myReviewButton);
        reviewsButton = findViewById(R.id.reviewsButton);
        directionsButton = findViewById(R.id.directionsButton);

        menuButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsRestaurantActivity.this, ListingMenuActivity.class);
                intent.putExtra(AndyConstants.Params.RID,rid);
                intent.putExtra(AndyConstants.Params.UID,preferenceHelper.getUID());
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });

        myReviewButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsRestaurantActivity.this, DetailsRatingActivity.class);
                intent.putExtra(AndyConstants.Params.RID,rid);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });

        reviewsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsRestaurantActivity.this, ListingRatingsActivity.class);
                intent.putExtra(AndyConstants.Params.RID,rid);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });

        directionsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent intent = new Intent(DetailsRestaurantActivity.this, DirectionsActivity.class);
                intent.putExtra(AndyConstants.Params.RID,rid);
                preferenceHelper.putActivityChanged(true);
                startActivity(intent);
            }
        });

    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }




    private class FetchDetailsAsyncTask extends AsyncTask<String, String, String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(DetailsRestaurantActivity.this)) {
                Toast.makeText(DetailsRestaurantActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(DetailsRestaurantActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {
            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.RESTAURANTDETAILS);
                final HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.UID, preferenceHelper.getUID());
                map.put(AndyConstants.Params.RID,rid);
                Log.d("rid:",rid);
                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("detailsjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            populateDetails(result);
        }
    }

    private void populateDetails(String result){
        Log.d("received rest info", result);

        if (parseContent.isSuccess(result)) {

            details = parseContent.getData(result);
            JSONObject dets = null;
            try {
                dets = details.getJSONObject(0);
                restaurant_name.setText(Html.fromHtml(dets.getString(AndyConstants.Params.RESTAURANT_NAME)));
                setTitle(Html.fromHtml(dets.getString(AndyConstants.Params.RESTAURANT_NAME)));
                description.setText(Html.fromHtml(dets.getString(AndyConstants.Params.DESCRIPTION)));
                cuisine.setText(Html.fromHtml(dets.getString(AndyConstants.Params.CUISINE)));
                open_time.setText(dets.getString(AndyConstants.Params.OPEN_TIME));
                close_time.setText(dets.getString(AndyConstants.Params.CLOSE_TIME));
                distance.setText(dets.getString(AndyConstants.Params.DISTANCE));
                email.setText(Html.fromHtml(dets.getString(AndyConstants.Params.EMAIL)));
                telephone_number.setText((dets.getString(AndyConstants.Params.TELEPHONE_NUMBER)));

                String addressString = Html.fromHtml(dets.getString(AndyConstants.Params.STREET))+ ", " +
                        Html.fromHtml(dets.getString(AndyConstants.Params.VILLAGE)) + ", "+
                        Html.fromHtml(dets.getString(AndyConstants.Params.PARISH));

                address.setText(addressString);

                String contactNameString = dets.getString(AndyConstants.Params.CONTACT_PERSON_F_NAME)+" "+dets.getString(AndyConstants.Params.CONTACT_PERSON_L_NAME);
                contact_person_name.setText(contactNameString);




            } catch (JSONException e) {
                e.printStackTrace();
            }



        }else {//if something goes wrong with the retrieval, put them back at the restaurant details class screen with an error toast
            Toast.makeText(DetailsRestaurantActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(DetailsRestaurantActivity.this, ListingRestaurantActivity.class);
            intent.putExtra(AndyConstants.Params.UID,preferenceHelper.getUID());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }

    }

}
