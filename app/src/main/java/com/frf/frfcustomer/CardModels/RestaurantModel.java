package com.frf.frfcustomer.CardModels;

import android.text.Html;

public class RestaurantModel {

    private String restaurant_name;
    private String cuisine;
    private String address;
    private String open_time;
    private String close_time;
    private String rid;
    private String distance;

    public String getRestaurantName() {
        return restaurant_name;
    }

    public void setRestaurantName(String restaurant_name) {
        this.restaurant_name = String.valueOf(Html.fromHtml(restaurant_name));
    }

    public String getCuisine() {
        return cuisine;
    }

    public void setCuisine(String cuisine) {
        this.cuisine = String.valueOf(Html.fromHtml(cuisine));
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = String.valueOf(Html.fromHtml(address));
    }

    public String getOpenTime() {
        return open_time;
    }

    public void setOpenTime(String open_time) {
        this.open_time = String.valueOf(Html.fromHtml(open_time));
    }

    public String getCloseTime() {
        return close_time;
    }

    public void setCloseTime(String close_time) {
        this.close_time = String.valueOf(Html.fromHtml(close_time));
    }

    public String getRID() {
        return rid;
    }

    public void setRID(String rid) {
        this.rid = String.valueOf(Html.fromHtml(rid));
    }

    public String getDistance() {
        return distance;
    }

    public void setDistance(String distance) {
        this.distance = String.valueOf(Html.fromHtml(distance));
    }
}
