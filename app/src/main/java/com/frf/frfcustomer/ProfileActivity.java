package com.frf.frfcustomer;

import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ExpandableListView;
import android.widget.Toast;

import androidx.appcompat.app.AppCompatActivity;

import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;
import com.frf.frfcustomer.ExpandableAdapters.DietPrefsExpandableListAdapter;
import com.frf.frfcustomer.ExpandableModels.DataItem;
import com.frf.frfcustomer.ExpandableModels.SubCategoryItem;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;

import static com.frf.frfcustomer.Core.MD5Password.getMD5EncryptedValue;

public class ProfileActivity extends AppCompatActivity implements IGPSActivity {

    private EditText username, f_name, l_name, password, email, telephone_number;
    private ExpandableListView dietTypeList;
    private Button updateButton;

    private ArrayList<DataItem> dietPrefsArCategory;
    private ArrayList<SubCategoryItem> dietPrefsArSubCategory;

    private ArrayList<HashMap<String, String>> dietPrefsParentItems;
    private ArrayList<ArrayList<HashMap<String, String>>> dietPrefsChildItems;
    private DietPrefsExpandableListAdapter dietPrefsExpandableListAdapter;

    private ParseContent parseContent;
    private PreferenceHelper preferenceHelper;

    private HashMap<String, String> map;

    private JSONObject general;
    private JSONArray dietPrefs, userDietPrefs;
    private JSONObject details;
    private String selectedDietPrefs;
    private int childHeight;

    //for location stuff
    private GPS gps;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_profile);

        parseContent = new ParseContent(this);
        preferenceHelper = new PreferenceHelper(this);

        gps = new GPS(this);

        username = findViewById(R.id.username);
        f_name = findViewById(R.id.f_name);
        l_name = findViewById(R.id.l_name);
        password = findViewById(R.id.password);
        email = findViewById(R.id.email);
        telephone_number = findViewById(R.id.telephone_number);

        map = new HashMap<>();

        map.put(AndyConstants.Params.UID, preferenceHelper.getUID());//get the user id

        updateButton = findViewById(R.id.updateButton);
        updateButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    update();
                } catch (IOException e) {
                    e.printStackTrace();
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
        });

        new FetchDetailsAsyncTask().execute();

    }


    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }




    /**
     * Fetches the details from the server
     */
    private class FetchDetailsAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(ProfileActivity.this)) {
                Toast.makeText(ProfileActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(ProfileActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEINDEX);

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("detailsjson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }


    private void setupReferences(String result) {

        Log.d("received refs", result);
        if (parseContent.isSuccess(result)) {
            details = parseContent.getDataObj(result);
        }else {//if something goes wrong with the retrieval, put them back at the restaurant listing screen with an error toast
            Toast.makeText(ProfileActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(ProfileActivity.this, ListingRestaurantActivity.class);
            intent.putExtra(AndyConstants.Params.UID,preferenceHelper.getUID());
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }
        Log.d("details", String.valueOf(details));

        try {
            general = details.getJSONArray("general").getJSONObject(0);
            username.setText(Html.fromHtml(general.getString(AndyConstants.Params.USERNAME)));
            f_name.setText(Html.fromHtml(general.getString(AndyConstants.Params.F_NAME)));
            l_name.setText(Html.fromHtml(general.getString(AndyConstants.Params.L_NAME)));
            email.setText(Html.fromHtml(general.getString(AndyConstants.Params.EMAIL)));
            telephone_number.setText(Html.fromHtml(general.getString(AndyConstants.Params.TELEPHONE_NUMBER)));
        } catch (JSONException e) {
            e.printStackTrace();
        }



//        //DIET PREFERENCES

        dietTypeList = findViewById(R.id.dietTypeList);

        DataItem dietPrefsItem = new DataItem();
        dietPrefsItem.setCategoryId("diet_preferences");
        dietPrefsItem.setCategoryName("Diet Preferences");

        dietPrefsArCategory = new ArrayList<>();
        dietPrefsArSubCategory = new ArrayList<>();
        dietPrefsParentItems = new ArrayList<>();
        dietPrefsChildItems = new ArrayList<>();

        try {
            dietPrefs = details.getJSONArray(AndyConstants.Params.DIET_PREFERENCES);
        } catch (JSONException e) {
            e.printStackTrace();
        }
        try {
            userDietPrefs = details.getJSONArray(AndyConstants.Params.USER_DIET_PREFERENCES);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        for(int i = 0; i < dietPrefs.length(); i++) {
            JSONObject dietPref = null;
            try {
                dietPref = dietPrefs.getJSONObject(i);
                SubCategoryItem subCategoryItem = new SubCategoryItem();
                subCategoryItem.setCategoryId("diet_preferences");
                subCategoryItem.setSubId(String.valueOf(dietPref.getInt(AndyConstants.Params.DT_ID)));

                for(int a=0; a < userDietPrefs.length(); a++){
                    JSONObject userDietPref = userDietPrefs.getJSONObject(a);
                    if(userDietPref.getInt(AndyConstants.Params.DT_ID) == dietPref.getInt(AndyConstants.Params.DT_ID)){//if the diet pref is one of the users preferred diets (from their profile)
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
                        break;
                    }
                    else{
                        subCategoryItem.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
                    }
                }
                subCategoryItem.setSubCategoryName(dietPref.getString(AndyConstants.Params.DT_NAME));
                dietPrefsArSubCategory.add(subCategoryItem);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        dietPrefsItem.setSubCategory(dietPrefsArSubCategory);
        dietPrefsArCategory.add(dietPrefsItem);

        for(DataItem data : dietPrefsArCategory){
//                        Log.i("Item", String.valueOf(arCategory));
            ArrayList<HashMap<String, String>> childArrayList =new ArrayList<HashMap<String, String>>();
            HashMap<String, String> mapParent = new HashMap<String, String>();

            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_ID,data.getCategoryId());
            mapParent.put(ExpandableConstantManager.Parameter.CATEGORY_NAME,data.getCategoryName());

            int countIsChecked = 0;
            for(SubCategoryItem subCategoryItem : data.getSubCategory()) {

                HashMap<String, String> mapChild = new HashMap<String, String>();
                mapChild.put(ExpandableConstantManager.Parameter.SUB_ID,subCategoryItem.getSubId());
                mapChild.put(ExpandableConstantManager.Parameter.SUB_CATEGORY_NAME,subCategoryItem.getSubCategoryName());
                mapChild.put(ExpandableConstantManager.Parameter.CATEGORY_ID,subCategoryItem.getCategoryId());
                mapChild.put(ExpandableConstantManager.Parameter.IS_CHECKED,subCategoryItem.getIsChecked());

                if(subCategoryItem.getIsChecked().equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {

                    countIsChecked++;
                }
                childArrayList.add(mapChild);
            }

            if(countIsChecked == data.getSubCategory().size()) {

                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE);
            }else {
                data.setIsChecked(ExpandableConstantManager.CHECK_BOX_CHECKED_FALSE);
            }

            mapParent.put(ExpandableConstantManager.Parameter.IS_CHECKED,data.getIsChecked());
            dietPrefsChildItems.add(childArrayList);
            dietPrefsParentItems.add(mapParent);

        }

        ExpandableConstantManager.parentItems = dietPrefsParentItems;
        ExpandableConstantManager.childItems = dietPrefsChildItems;

        Log.d("TAG", "dietPrefs parent: "+dietPrefsParentItems);
        Log.d("TAG", "dietPrefs children: "+dietPrefsChildItems);

        dietPrefsExpandableListAdapter = new DietPrefsExpandableListAdapter(this,dietPrefsParentItems,dietPrefsChildItems,false);

        dietTypeList.setAdapter(dietPrefsExpandableListAdapter);

        Log.d("TAG", "dietPrefs parent: "+dietPrefsParentItems);
        Log.d("TAG", "dietPrefs children: "+dietPrefsChildItems);

        dietTypeList.setOnGroupExpandListener(new ExpandableListView.OnGroupExpandListener() {
            @Override
            public void onGroupExpand(int groupPosition) {
                int height = 0;
                Log.d("dietType children","count= "+dietTypeList.getChildCount());
                for (int i = 0; i < dietTypeList.getChildCount(); i++) {
                    childHeight = dietTypeList.getChildAt(i).getMeasuredHeight();
                    height = childHeight + dietTypeList.getDividerHeight();
                }

                Log.d("count", String.valueOf(dietPrefsExpandableListAdapter.getChildrenCount(groupPosition)));
                dietTypeList.getLayoutParams().height = (height) * (dietPrefsExpandableListAdapter.getChildrenCount(groupPosition)+1);
            }
        });

        // Listview Group collapsed listener
        dietTypeList.setOnGroupCollapseListener(new ExpandableListView.OnGroupCollapseListener() {

            @Override
            public void onGroupCollapse(int groupPosition) {
                dietTypeList.getLayoutParams().height = childHeight;
            }
        });

    }


    public void update()throws IOException, JSONException {

        if (!AndyUtils.isNetworkAvailable(ProfileActivity.this)) {
            Toast.makeText(ProfileActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
            return;
        }
        AndyUtils.showSimpleProgressDialog(ProfileActivity.this);

        map.put(AndyConstants.Params.USERNAME, username.getText().toString());
        map.put(AndyConstants.Params.F_NAME, f_name.getText().toString());
        map.put(AndyConstants.Params.L_NAME, l_name.getText().toString());
        map.put(AndyConstants.Params.EMAIL, email.getText().toString());
        map.put(AndyConstants.Params.TELEPHONE_NUMBER, telephone_number.getText().toString());

        //deal with password
        String pwd = password.getText().toString();
        String encryptedPassword;
        if(pwd.isEmpty()){//if they didn't enter a password use the original encrypted password that's stored in preference helper
            encryptedPassword = preferenceHelper.getPassword();
        }
        else{
            encryptedPassword  = getMD5EncryptedValue(password.getText().toString());
        }

        map.put(AndyConstants.Params.PASSWORD, encryptedPassword);

        //selectedDietPrefs
        for (int i = 0; i < DietPrefsExpandableListAdapter.parentItems.size(); i++) {
            ArrayList<Integer> children = new ArrayList<Integer>();

            for (int j = 0; j < DietPrefsExpandableListAdapter.childItems.get(i).size(); j++) {

                String isChildChecked = DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.IS_CHECKED);
                Integer childID = Integer.parseInt(DietPrefsExpandableListAdapter.childItems.get(i).get(j).get(ExpandableConstantManager.Parameter.SUB_ID));

                if (isChildChecked.equalsIgnoreCase(ExpandableConstantManager.CHECK_BOX_CHECKED_TRUE)) {
                    children.add(childID);
                }
            }
            selectedDietPrefs = String.valueOf(children);
            Log.d("selectedDietPrefs",selectedDietPrefs);
        }

        map.put(AndyConstants.Params.DIET_PREFERENCES,selectedDietPrefs);

        //get the user's current location and map the coordinates
        String longitude = preferenceHelper.getLongitude();
        map.put(AndyConstants.Params.LONGITUDE, String.valueOf(longitude));
        String latitude = preferenceHelper.getLatitude();
        map.put(AndyConstants.Params.LATITUDE, String.valueOf(latitude));

        new AsyncTask<Void, Void, String>(){
            protected String doInBackground(Void[] params) {
                String response="";
                try {
                    HttpRequest req = new HttpRequest(AndyConstants.ServiceType.PROFILEUPDATE);

                    response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                } catch (Exception e) {
                    response=e.getMessage();
                }
                return response;
            }
            protected void onPostExecute(String result) {
                //do something with response
                Log.d("res:", result);
                onTaskCompleted(result);
            }
        }.execute();
    }
    private void onTaskCompleted(String response) {
        Log.d("responsejson", response.toString());
        AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog

        if (parseContent.isSuccess(response)) {


            try {
                JSONObject newInfo = parseContent.getDataObj(response).getJSONArray("general").getJSONObject(0);
                String newPassword =  newInfo.getString(AndyConstants.Params.PASSWORD);
                String newUsername =  newInfo.getString(AndyConstants.Params.USERNAME);

                //if the password or username was updated
                if(!newPassword.equals(preferenceHelper.getPassword()) || !newUsername.equals(preferenceHelper.getUsername())){
                    Toast.makeText(ProfileActivity.this, "Update Successful! Please login with new credentials.", Toast.LENGTH_SHORT).show();

                    preferenceHelper.clearPrefs();//clear all preferences (log them out)

                    Intent intent = new Intent(ProfileActivity.this, LoginActivity.class);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    preferenceHelper.putActivityChanged(true);
                    startActivity(intent);
                    this.finish();
                }
                else{//if the password and username weren't changed
                    Toast.makeText(ProfileActivity.this, "Update Successful!", Toast.LENGTH_SHORT).show();

                    new FetchDetailsAsyncTask().execute();
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

        }else {//if there was a problem make a toast with the error message
            Toast.makeText(ProfileActivity.this, parseContent.getErrorMessage(response), Toast.LENGTH_SHORT).show();
        }
    }


}
