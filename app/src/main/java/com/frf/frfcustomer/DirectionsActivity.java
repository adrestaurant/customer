package com.frf.frfcustomer;

import android.content.Intent;
import android.graphics.Color;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.provider.Settings;
import android.text.Html;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.FragmentActivity;

import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.Core.AndyUtils;
import com.frf.frfcustomer.Core.GPS;
import com.frf.frfcustomer.Core.HttpRequest;
import com.frf.frfcustomer.Core.IGPSActivity;
import com.frf.frfcustomer.Core.ParseContent;
import com.frf.frfcustomer.Core.PreferenceHelper;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


public class DirectionsActivity extends FragmentActivity implements OnMapReadyCallback, IGPSActivity {

    private GPS gps;

    private PreferenceHelper preferenceHelper;
    private ParseContent parseContent;

    private double restaurantLat, restaurantLng, userLat, userLng;
    private String addr, restaurant_name, rid, str_origin, str_dest, selectedMode;
    private LatLng origin, destination;
    private JSONArray details;

    private TextView address, modeString;
    private Button walkingButton, drivingButton;
    private GoogleMap mMap;

    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_directions);

        gps = new GPS(this);

        preferenceHelper = new PreferenceHelper(this);
        parseContent = new ParseContent(this);

        Intent intent = getIntent();
        rid = intent.getStringExtra(AndyConstants.Params.RID);

        address = findViewById(R.id.address);
        modeString = findViewById(R.id.modeString);

        walkingButton = findViewById(R.id.walkingButton);
        drivingButton = findViewById(R.id.drivingButton);

        selectedMode = "driving";
        modeString.setText("Mode: driving");


        walkingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedMode = "walking";
                modeString.setText("Mode: walking");
                new FetchLocationAsyncTask().execute();
            }
        });

        drivingButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectedMode = "driving";
                modeString.setText("Mode: driving");
                new FetchLocationAsyncTask().execute();
            }
        });

        new FetchLocationAsyncTask().execute();
    }

    //for location stuff
    @Override
    protected void onResume() {
        if(!gps.isRunning()) gps.resumeGPS();
        super.onResume();
    }

    @Override
    protected void onStop() {
        gps.stopGPS();
        super.onStop();
    }

    @Override
    public void displayGPSSettingsDialog() {
        Intent intent = new Intent(Settings.ACTION_LOCATION_SOURCE_SETTINGS);
        startActivity(intent);
    }

    private class FetchLocationAsyncTask extends AsyncTask<String, String, String> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            if (!AndyUtils.isNetworkAvailable(DirectionsActivity.this)) {
                Toast.makeText(DirectionsActivity.this, "Internet is required!", Toast.LENGTH_SHORT).show();
                return;
            }
            //Display progress bar
            AndyUtils.showSimpleProgressDialog(DirectionsActivity.this);
        }

        @Override
        protected String doInBackground(String... params) {

            String response="";
            try {
                HttpRequest req = new HttpRequest(AndyConstants.ServiceType.RESTAURANTDETAILS);
                HashMap<String, String> map = new HashMap<>();
                map.put(AndyConstants.Params.RID, rid);
                map.put(AndyConstants.Params.UID,preferenceHelper.getUID());

                response = req.withHeaders("Authorization:Basic " + preferenceHelper.getUserPass()).prepare(HttpRequest.Method.POST).withData(map).sendAndReadString();
                Log.d("response", response);
            } catch (Exception e) {
                response=parseContent.getErrorMessage(response);
            }
            return response;
        }

        protected void onPostExecute(String result) {
            Log.d("detailsson", result);
            AndyUtils.removeSimpleProgressDialog();  //will remove progress dialog
            setupReferences(result);
        }

    }

    private void setupReferences(String result){
        Log.d("received rest info", result);

        if (parseContent.isSuccess(result)) {

            details = parseContent.getData(result);
            JSONObject dets = null;
            try {
                dets = details.getJSONObject(0);
                restaurant_name = String.valueOf(Html.fromHtml(dets.getString(AndyConstants.Params.RESTAURANT_NAME)));
                setTitle("Directions to  "+restaurant_name);

                restaurantLat = Double.parseDouble(dets.getString(AndyConstants.Params.LATITUDE));
                restaurantLng = Double.parseDouble(dets.getString(AndyConstants.Params.LONGITUDE));

                destination = new LatLng(restaurantLat,restaurantLng);


                addr = Html.fromHtml(dets.getString(AndyConstants.Params.STREET))+ ", " +
                        Html.fromHtml(dets.getString(AndyConstants.Params.VILLAGE)) + ", "+
                        Html.fromHtml(dets.getString(AndyConstants.Params.PARISH));

                address.setText("Restaurant: "+restaurant_name+"\nAddress: "+addr);

                Log.d("restauranttLat", String.valueOf(restaurantLat));
                Log.d("restaurantLng", String.valueOf(restaurantLng));
                Log.d("destination", String.valueOf(destination));
                Log.d("address", String.valueOf(address.getText()));
            } catch (JSONException e) {
                e.printStackTrace();
            }

            // Obtain the SupportMapFragment and get notified when the map is ready to be used.
            SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
                    .findFragmentById(R.id.map);
            mapFragment.getMapAsync(this);

        }else {//if something goes wrong with the retrieval, put them at the restaurant details screen with an error toast
            Toast.makeText(DirectionsActivity.this, parseContent.getErrorMessage(result), Toast.LENGTH_SHORT).show();
            Intent intent = new Intent(DirectionsActivity.this, DetailsRestaurantActivity.class);
            intent.putExtra(AndyConstants.Params.RID,rid);
            intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
            preferenceHelper.putActivityChanged(true);
            startActivity(intent);
            this.finish();
        }

    }

    /**
     * Manipulates the map once available.
     * This callback is triggered when the map is ready to be used.
     * This is where we can add markers or lines, add listeners or move the camera.
     */
    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;

        mMap.setMapType(GoogleMap.MAP_TYPE_NORMAL);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setCompassEnabled(true);

        //get the user's coordinated to set the origin point
        userLat = Double.parseDouble(preferenceHelper.getLatitude());
        userLng = Double.parseDouble(preferenceHelper.getLongitude());
        origin = new LatLng(userLat,userLng);

        // Add a marker at current coordinates location and move the camera
        mMap.addMarker(new MarkerOptions().position(origin).title("Your Coordinates").snippet("Latitude:"+userLat+"\nLongitude: "+userLng).draggable(false)).showInfoWindow();
        mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(origin,16));

        googleMap.setInfoWindowAdapter(new GoogleMap.InfoWindowAdapter() {

            @Override
            public View getInfoWindow(Marker arg0) {
                return null;
            }

            @Override
            public View getInfoContents(Marker marker) {

                LinearLayout info = new LinearLayout(DirectionsActivity.this);
                info.setOrientation(LinearLayout.VERTICAL);

                TextView title = new TextView(DirectionsActivity.this);
                title.setTextColor(Color.BLACK);
                title.setGravity(Gravity.CENTER);
                title.setTypeface(null, Typeface.BOLD);
                title.setText(marker.getTitle());

                TextView snippet = new TextView(DirectionsActivity.this);
                snippet.setTextColor(Color.GRAY);
                snippet.setText(marker.getSnippet());

                info.addView(title);
                info.addView(snippet);

                return info;
            }
        });

        // Getting URL to the Google Directions API
        getDirectionsUrl();
    }


    private void getDirectionsUrl() {

        // Origin of route
        str_origin = "origin=" + userLat + "," + userLng;

        // Destination of route
        str_dest = "destination="+ restaurantLat + "," + restaurantLng;

        // Sensor enabled
        String sensor = "sensor=false";
        String mode = "mode="+selectedMode;
        String key = "key="+AndyConstants.Params.API_KEY;

        // Building the parameters to the web service
        String parameters = str_origin + "&" + str_dest + "&" + sensor + "&" + mode + "&" + key;

        // Output format
        String output = "json";

        // Building the url to the web service
        String url = "https://maps.googleapis.com/maps/api/directions/" + output + "?" + parameters;

        DownloadTask downloadTask = new DownloadTask();

        // Start downloading json data from Google Directions API
        downloadTask.execute(url);
    }



    private class DownloadTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... url) {

            String data = "";

            try {
                Log.d("dwnldtask url", String.valueOf(url));
                Log.d("dwnldtask url[0]", url[0]);
                data = downloadUrl(url[0]);
                Log.d("dwnldtask data",data);
            } catch (Exception e) {
                Log.d("Background Task", e.toString());
            }
            return data;
        }

        @Override
        protected void onPostExecute(String result) {
            super.onPostExecute(result);

            Log.d("dwnldtsk result",result);

            JSONObject jsonObject = null;
            try {
                jsonObject = new JSONObject(result);

                if (!jsonObject.has("error_message")){
                    ParserTask parserTask = new ParserTask();
                    parserTask.execute(result);
                }
                else {
                    Toast.makeText(DirectionsActivity.this, jsonObject.getString("error_message"), Toast.LENGTH_SHORT).show();
                    Intent intent = new Intent(DirectionsActivity.this, DetailsRestaurantActivity.class);
                    intent.putExtra(AndyConstants.Params.RID,rid);
                    intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_NEW_TASK);
                    preferenceHelper.putActivityChanged(true);
                    startActivity(intent);
                    DirectionsActivity.this.finish();
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }




        }
    }

    /**
     * A method to download json data from url
     */
    private String downloadUrl(String strUrl) throws IOException {
        String data = "";
        InputStream iStream = null;
        HttpURLConnection urlConnection = null;
        try {
            URL url = new URL(strUrl);

            urlConnection = (HttpURLConnection) url.openConnection();

            urlConnection.connect();

            iStream = urlConnection.getInputStream();

            BufferedReader br = new BufferedReader(new InputStreamReader(iStream));

            StringBuffer sb = new StringBuffer();

            String line = "";
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

            data = sb.toString();

            br.close();

        } catch (Exception e) {
            Log.d("Exception", e.toString());
        } finally {
            iStream.close();
            urlConnection.disconnect();
        }
        return data;
    }


    /**
     * A class to parse the Google Places in JSON format
     */
    private class ParserTask extends AsyncTask<String, Integer, List<List<HashMap<String, String>>>> {

        // Parsing the data in non-ui thread
        @Override
        protected List<List<HashMap<String, String>>> doInBackground(String... jsonData) {

            JSONObject jObject;
            List<List<HashMap<String, String>>> routes = null;

            try {
                jObject = new JSONObject(jsonData[0]);
                DirectionsJSONParser parser = new DirectionsJSONParser();

                routes = parser.parse(jObject);
            } catch (Exception e) {
                e.printStackTrace();
            }
            return routes;
        }

        @Override
        protected void onPostExecute(List<List<HashMap<String, String>>> result) {
            ArrayList points = null;
            PolylineOptions lineOptions = null;
            MarkerOptions markerOptions = new MarkerOptions();

            for (int i = 0; i < result.size(); i++) {
                points = new ArrayList();
                lineOptions = new PolylineOptions();

                List<HashMap<String, String>> path = result.get(i);

                for (int j = 0; j < path.size(); j++) {
                    HashMap<String, String> point = path.get(j);

                   double lat = Double.parseDouble(point.get("lat"));
                    double lng = Double.parseDouble(point.get("lng"));
                    LatLng position = new LatLng(lat, lng);
                    points.add(position);
                }
                lineOptions.addAll(points);
                lineOptions.width(12);
                lineOptions.color(Color.RED);
                lineOptions.geodesic(true);

            }

            mMap.clear();

            mMap.addMarker(new MarkerOptions().position(origin).title("Your Coordinates").snippet("Latitude:"+userLat+"\nLongitude: "+userLng).draggable(false)).showInfoWindow();
            mMap.addMarker(new MarkerOptions().position(destination).title("Restaurant Coordinates").snippet("Latitude:"+restaurantLat+"\nLongitude: "+restaurantLng).draggable(false)).showInfoWindow();
            mMap.moveCamera(CameraUpdateFactory.newLatLngZoom(origin,16));



// Drawing polyline in the Google Map for the i-th route
            mMap.addPolyline(lineOptions);
        }
    }
}