package com.frf.frfcustomer.CardAdapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfcustomer.CardModels.DishModel;
import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class DishCardAdapter extends RecyclerView.Adapter<DishCardAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<DishModel> dishModelArrayList;


    public DishCardAdapter(Context ctx, ArrayList<DishModel> dishModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.dishModelArrayList = dishModelArrayList;
    }

    @Override
    public DishCardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.card_item_dish, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(DishCardAdapter.MyViewHolder holder, int position) {

        holder.dish_name.setText(dishModelArrayList.get(position).getDishName());
        holder.food_category.setText(dishModelArrayList.get(position).getFoodCategory());
        holder.cuisine.setText(dishModelArrayList.get(position).getCuisine());
        holder.diet_type.setText(dishModelArrayList.get(position).getDietType());
        holder.price.setText(dishModelArrayList.get(position).getPrice());
        Picasso.get().load(Uri.parse(AndyConstants.ServiceType.IMAGES + dishModelArrayList.get(position).getImageUrl()))
                .resize(120, 60).placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder_error).into(holder.image_url);
        holder.d_id.setText(dishModelArrayList.get(position).getDID());

    }

    @Override
    public int getItemCount() {
        return dishModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView dish_name;
        TextView food_category;
        TextView cuisine;
        TextView diet_type;
        TextView price;
        ImageView image_url;
        TextView d_id;


        public MyViewHolder(View itemView) {
            super(itemView);

            dish_name = itemView.findViewById(R.id.dish_name);
            food_category = itemView.findViewById(R.id.food_category);
            cuisine = itemView.findViewById(R.id.cuisine);
            diet_type = itemView.findViewById(R.id.diet_type);
            price = itemView.findViewById(R.id.price);
            image_url = itemView.findViewById(R.id.image_url);
            d_id = itemView.findViewById(R.id.d_id);

        }

    }
}

