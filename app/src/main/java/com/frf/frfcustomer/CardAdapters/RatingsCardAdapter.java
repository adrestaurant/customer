package com.frf.frfcustomer.CardAdapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RatingBar;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfcustomer.CardModels.RatingsModel;
import com.frf.frfcustomer.R;

import java.util.ArrayList;

public class RatingsCardAdapter extends RecyclerView.Adapter<RatingsCardAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<RatingsModel> ratingsModelArrayList;


    public RatingsCardAdapter(Context ctx, ArrayList<RatingsModel> ratingsModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.ratingsModelArrayList = ratingsModelArrayList;
    }

    @Override
    public RatingsCardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.card_item_rating, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RatingsCardAdapter.MyViewHolder holder, int position) {

        holder.username.setText(ratingsModelArrayList.get(position).getUsername());
        holder.rating.setIsIndicator(true);
        holder.rating.setRating(Float.parseFloat(ratingsModelArrayList.get(position).getRating()));

        if(!ratingsModelArrayList.get(position).getComment().isEmpty()) {
            holder.commentLayout.setVisibility(View.VISIBLE);
            holder.comment.setText(ratingsModelArrayList.get(position).getComment());
        }

        holder.created_at.setText(ratingsModelArrayList.get(position).getCreatedAt());
        holder.updated_at.setText(ratingsModelArrayList.get(position).getUpdatedAt());

    }

    @Override
    public int getItemCount() {
        return ratingsModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView username;
        RatingBar rating;
        LinearLayout commentLayout;
        TextView comment;
        TextView created_at;
        TextView updated_at;


        public MyViewHolder(View itemView) {
            super(itemView);

            username = itemView.findViewById(R.id.username);
            rating = itemView.findViewById(R.id.rating);
            commentLayout = itemView.findViewById(R.id.commentLayout);
            comment = itemView.findViewById(R.id.comment);
            created_at = itemView.findViewById(R.id.created_at);
            updated_at = itemView.findViewById(R.id.updated_at);

        }

    }
}
