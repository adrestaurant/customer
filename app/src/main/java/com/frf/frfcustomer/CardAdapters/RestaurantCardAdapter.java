package com.frf.frfcustomer.CardAdapters;


import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfcustomer.CardModels.RestaurantModel;
import com.frf.frfcustomer.R;

import java.util.ArrayList;

public class RestaurantCardAdapter extends RecyclerView.Adapter<RestaurantCardAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<RestaurantModel> restaurantModelArrayList;


    public RestaurantCardAdapter(Context ctx, ArrayList<RestaurantModel> restaurantModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.restaurantModelArrayList = restaurantModelArrayList;
    }

    @Override
    public RestaurantCardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.card_item_restaurant, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(RestaurantCardAdapter.MyViewHolder holder, int position) {

        holder.restaurant_name.setText(restaurantModelArrayList.get(position).getRestaurantName());
        holder.cuisine.setText(restaurantModelArrayList.get(position).getCuisine());
        holder.address.setText(restaurantModelArrayList.get(position).getAddress());
        holder.open_time.setText(restaurantModelArrayList.get(position).getOpenTime());
        holder.close_time.setText(restaurantModelArrayList.get(position).getCloseTime());
        holder.rid.setText(restaurantModelArrayList.get(position).getRID());
        holder.distance.setText(restaurantModelArrayList.get(position).getDistance());

    }

    @Override
    public int getItemCount() {
        return restaurantModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView restaurant_name;
        TextView cuisine;
        TextView address;
        TextView open_time;
        TextView close_time;
        TextView rid;
        TextView distance;


        public MyViewHolder(View itemView) {
            super(itemView);

           restaurant_name = itemView.findViewById(R.id.restaurant_name);
           cuisine = itemView.findViewById(R.id.cuisine);
           address = itemView.findViewById(R.id.address);
           open_time = itemView.findViewById(R.id.open_time);
           close_time = itemView.findViewById(R.id.close_time);
           rid = itemView.findViewById(R.id.rid);
           distance = itemView.findViewById(R.id.distance);

        }

    }
}

