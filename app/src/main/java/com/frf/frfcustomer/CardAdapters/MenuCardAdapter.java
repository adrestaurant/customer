package com.frf.frfcustomer.CardAdapters;

import android.content.Context;
import android.net.Uri;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.frf.frfcustomer.CardModels.MenuModel;
import com.frf.frfcustomer.Core.AndyConstants;
import com.frf.frfcustomer.R;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;

public class MenuCardAdapter extends RecyclerView.Adapter<MenuCardAdapter.MyViewHolder> {

    private LayoutInflater inflater;
    private ArrayList<MenuModel> menuModelArrayList;


    public MenuCardAdapter(Context ctx, ArrayList<MenuModel> menuModelArrayList){

        inflater = LayoutInflater.from(ctx);
        this.menuModelArrayList = menuModelArrayList;
    }

    @Override
    public MenuCardAdapter.MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = inflater.inflate(R.layout.card_item_menu, parent, false);
        MyViewHolder holder = new MyViewHolder(view);

        return holder;
    }

    @Override
    public void onBindViewHolder(MenuCardAdapter.MyViewHolder holder, int position) {

        holder.meal_name.setText(menuModelArrayList.get(position).getMealName());
        holder.diet_type.setText(menuModelArrayList.get(position).getDietType());
        holder.cuisine.setText(menuModelArrayList.get(position).getCuisine());
        holder.start_time.setText(menuModelArrayList.get(position).getStartTime());
        holder.end_time.setText(menuModelArrayList.get(position).getEndTime());
        Picasso.get().load(Uri.parse(AndyConstants.ServiceType.IMAGES + menuModelArrayList.get(position).getImageUrl()))
                .resize(120, 60).placeholder(R.drawable.image_placeholder).error(R.drawable.image_placeholder_error).into(holder.image_url);
        holder.menu_id.setText(menuModelArrayList.get(position).getMenuID());

    }

    @Override
    public int getItemCount() {
        return menuModelArrayList.size();
    }

    class MyViewHolder extends RecyclerView.ViewHolder{

        TextView meal_name;
        TextView diet_type;
        TextView cuisine;
        TextView start_time;
        TextView end_time;
        ImageView image_url;
        TextView menu_id;


        public MyViewHolder(View itemView) {
            super(itemView);

            meal_name = itemView.findViewById(R.id.meal_name);
            diet_type = itemView.findViewById(R.id.diet_type);
            cuisine = itemView.findViewById(R.id.cuisine);
            start_time = itemView.findViewById(R.id.start_time);
            end_time = itemView.findViewById(R.id.end_time);
            image_url = itemView.findViewById(R.id.image_url);
            menu_id = itemView.findViewById(R.id.menu_id);

        }

    }

}
